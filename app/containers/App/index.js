/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React from 'react';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import HomePage from 'containers/HomePage/Loadable';
import FeaturePage from 'containers/FeaturePage/Loadable';

import AppDetails from 'containers/AppDetails/Loadable';
import Configurator from 'containers/Configurator/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import Header from 'components/Header';
import Footer from 'components/Footer';
import { appConst, userConfig } from 'configs/appConfig';
import '../../utils/configureLogger';

const AppWrapper = styled.div`
  width: 100%;
`;

const basename = '/';

// if(process.env.NODE_ENV === 'production') {
//   basename = '/build';
// }
export default function App(props) {
  const routes = [{
    path: '/',
    component: AppDetails,
  }];

  return !userConfig.runAsPlugin ? (
    <AppWrapper>
      <Helmet
        titleTemplate="%s - React.js Boilerplate"
        defaultTitle="React.js Boilerplate"
      >
        <meta name="description" content="A React.js Boilerplate application" />
      </Helmet>

      <BrowserRouter basename={basename}>
        <Switch>
          {
          routes.map((route, index) => <Route key={index} exact path={route.path} component={route.component} />)
        }
          <Route path="" component={NotFoundPage} />
          {/*
        This is to be applied when we want the children routes for AppDetails
          <AppDetails>
            <Route path="/app/some-url" component={some component} />
          </AppDetails>
        */}
        </Switch>
      </BrowserRouter>
    </AppWrapper>
  ) : (<AppDetails {...props} />);
}
