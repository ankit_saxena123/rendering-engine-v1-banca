/**
 *
 * RenderConfirmMpin
 *
 */

import React from 'react';

import * as validationFns from '../../utilities/validations/validationFns';
import * as userUtils from '../../utilities/userUtils';
import { strings } from '../../../locales/i18n';

import { sha256, sha224 } from 'js-sha256';
import { setClientKey } from '../../configs/appConstants';

export default class RenderConfirmMpinAbstract extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.state = {
      mpin: '',
      isMpinValid: '',
      invalidMpinError: '',
      isTouchIDSupported: false,
    };
  }

  checkMpinValidation = (value) => {
    this.setState({ mpin: value }, this.submitMpin);
  }

  // handleMpinChange = (e) => {
  //   this.setState({mpin: e.target.value});
  //   let value = e.target.value;
  //   // Check the mobile validation and set the state
  //   window.setTimeout(() => this.checkMpinValidation(value));
  // }

  getPopUpdata = () => {
    const self = this;
    return {
      headerText: strings('login.touchIdPopupHead'),
      bodyText: strings('login.touchIdPopupBody'),
      actionButtons: [{
        label: strings('login.touchIdPopupSkip'),
        callback: () => self.shouldEnableTouchID(false),
        action: 'callback',
        type: 'secondary',
      },
      {
        label: strings('login.touchIdPopupUse'),
        callback: () => self.shouldEnableTouchID(true),
        action: 'callback',
        type: 'primary',
      }],
      type: 'touch',
      showPopup: true,
    };
  }

  setMpinToStorage = () => {
    this.props.setUserDetails({
      ...this.props.userDetails,
      mpin: this.state.mpin,
      mpinConfirmed: 'true',
    });
    this.props.toggleMpin(true);
  }

  submitMpin = () => {
    const mpinObj = {
      params: { mPin: sha256(this.state.mpin) },
      userDetails: this.props.userDetails,
      successCb: (response) => {
        const responseData = response.data;
        setClientKey({
          hypervergeFb: {
            appid: responseData.hypervergeAppId,
            appkey: responseData.hypervergeAppSecret,
          },
        });
        if (!this.props.userDetails.mpin && this.state.isTouchIDSupported) {
          this.showTouchIDPopUp();
        } else {
          this.setMpinToStorage();
        }
      },
      errorCb: () => {
        this.setState({ invalidMpinError: strings('errors.mpin') });
      },
    };
    this.props.postRequest({ key: 'validateMpin', data: mpinObj });
  }

  // TODO: for testing purpose only - remove it
  logout = () => {
    // Clear the session/AsyncStorage Details for the user
    userUtils.logout().then((userObj) => {
      const userDetails = userObj;
      this.props.setUserDetails(userDetails);
      this.props.clearJourneyData();
      this.props.toggleMpin(false);
    });
  }


  render() {
    return null;
  }
}
