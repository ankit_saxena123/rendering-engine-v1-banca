/**
 *
 * Payment
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { View, TouchableHighlight, Text } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { colors } from '../../configs/styleVars';

import RenderSubHeader from '../../components/RenderSubHeader';
import RenderDetailList from '../../components/RenderDetailList';
import RenderLabel from '../../components/RenderLabel';
import * as inactivityTracker from '../../utilities/inactivityTracker';


import {
  payListStyle,
  buttonWrapperStyle,
  buttonFontStyle,
  centerListStyle,
  midSectionStyle,
  paidStatusStyle,
  listItemStyle,
  dueStatusStyle,
} from './style';
const paymnetInfo = [
  {
    id: 'lastPaymentDate',
    label: 'Last Payment Date:',
    value: '-',
  },
  {
    id: 'lastPaymentAmount',
    label: 'Last Payment Amount:',
    value: '-',
  },
  {
    id: 'overDueAmount',
    label: 'Over Due Amount:',
    value: '-',
  },
  {
    id: 'nextPaymentDueDate',
    label: 'Next Payment Due Date:',
    value: '-',
  },
  {
    id: 'nextPaymentAmount',
    label: 'Next Payment Amount:',
    value: '-',
  },
  {
    id: 'totalLoanOutstandin',
    label: 'Total Loan Outstanding',
    value: '-',
  },
];

const paymentSchedule = [];

for (let i = 0; i < 11; i += 1) {
  const obj = {
    date: '-',
    installment: '-',
    status: '',
  };
  paymentSchedule.push(obj);
}

export class Payment extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showPaymentSchdule: false,
    };
    this.resetInactivity = this.resetInactivity.bind(this);
  }

  componentDidMount() {
    this.resetInactivity();
  }

  componentWillUnmount() {
    inactivityTracker.clearTracker();
  }

  toggleMpin = () => {
    this.props.screenProps.toggleMpin(false);
  }

  // logs out user in case of inactivity for x min
  resetInactivity() {
    const self = this;
    inactivityTracker.resetInactivity(this.toggleMpin);
  }

  onButtonPress = () => {
    this.resetInactivity();
    this.setState(({ showPaymentSchdule }) => ({
      showPaymentSchdule: !showPaymentSchdule,
    }));
  }

  getPaymentScheduleList = () => {
    const dateList = [<RenderLabel type={'title'} key={'date'}>{'Date'}</RenderLabel>];
    const installmentList = [<RenderLabel type={'title'} key={'installment'}>{'Monthly Installment'}</RenderLabel>];
    const statusList = [<RenderLabel type={'title'} key={'status'}>{'Status'}</RenderLabel>];
    paymentSchedule.forEach((detail, index) => {
      const statusStyle = detail.status === 'Paid' ? paidStatusStyle : dueStatusStyle;
      dateList.push(
        <RenderLabel type={'value'} style={listItemStyle} key={`date_${index}`}>
          {detail.date}
        </RenderLabel>
        );
      installmentList.push(
        <RenderLabel type={'value'} style={listItemStyle} key={`installment_${index}`}>
          {detail.installment}
        </RenderLabel>
      );
      statusList.push(
        <RenderLabel type={'value'} style={{ ...statusStyle, ...listItemStyle }} key={`status_${index}`}>
          { detail.status || '-'}</RenderLabel>
      );
    });
    return [dateList, installmentList, statusList];
  }
  render() {
    let pageTitle = 'Payments';
    let midSection = <RenderDetailList detailList={paymnetInfo} />;
    let buttonText = 'View Payment Schedule';
    const [dateList, installmentList, statusList] = this.getPaymentScheduleList();
    if (this.state.showPaymentSchdule) {
      pageTitle = 'Payment Schedule';
      midSection = (
        <View style={midSectionStyle}>
          <View>
            {dateList}
          </View>
          <View style={centerListStyle}>
            {installmentList}
          </View>
          <View>
            {statusList}
          </View>
        </View>
      );
      buttonText = 'View Payments';
    }
    return (
      <View>
        {this.props.screenProps.appHeader}
        <RenderSubHeader>{pageTitle}</RenderSubHeader>
        <View style={payListStyle}>
          {midSection}
        </View>
        <View>
          <TouchableHighlight
            onPress={this.onButtonPress}
            underlayColor={colors.white}
          >
            <View style={buttonWrapperStyle}>
              <Text style={buttonFontStyle}>{buttonText}</Text>
              <Icon style={buttonFontStyle} name="chevron-right" />
            </View>
          </TouchableHighlight>
        </View>
      </View>
    );
  }
}

Payment.propTypes = {
  dispatch: PropTypes.func.isRequired,
};


function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(null, mapDispatchToProps);

export default compose(
  withConnect,
)(Payment);
