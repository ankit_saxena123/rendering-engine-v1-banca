/**
 *
 * Login Native
 *
 */


import React from 'react';

import TextInputField from '../../components/TextInputField';
import RaisedButton from 'material-ui/RaisedButton';
import RenderButton from '../../components/RenderButton';
import LoginWrapper from './LoginWrapper';
import LoginAbstract from './LoginAbstract';
import RenderOtp from '../RenderOtp';
import ActionWrapper from './ActionWrapper';
import Row from '../../components/Row';
import Column from '../../components/Column';
import { colors, brand } from '../../configs/styleVars';


export default class LoginContainer extends LoginAbstract { // eslint-disable-line react/prefer-stateless-function

  showOtpField = () => {
    this.setState({ otpGenerated: true });
  }

  handleGenerateOtp = () => {
    this.generateOtp(this.showOtpField);
  }

  render() {
    const errorMessage = this.state.invalidMobileError ? this.state.invalidMobileError : null;
    const otpErrorMessage = this.state.invalidOtpError ? this.state.invalidOtpError : null;
    const componentProps = {
      id: 'ku-number-' + 'login',
      type: 'number',
      placeholder: 'Enter Mobile Number',
      floatingLabelFixed: true,
      floatingLabelText: 'Mobile Number',
    };
    const otpComponentProps = {
      id: 'ku-number-otp',
      type: 'number',
      floatingLabelText: 'OTP',
      floatingLabelFixed: true,
      placeholder: 'Please enter OTP',
    };
    const errorBlock = null;
    let otpBlock = null;
    let sendOtpButton = (
      <div className="center-align">
        <RenderButton
          type="primary"
          onClick={this.handleGenerateOtp}
          label="SEND OTP"
          disabled={!this.state.isMobileNumberValid}
        />
      </div>
    );

    // Check if OTP is generated and only then show Login button and OTP input
    if (this.state.otpGenerated) {
      otpBlock = (
        <div>
          <div className="center-align">
            <TextInputField
              componentProps={otpComponentProps}
              errorMessage={otpErrorMessage}
              onChangeHandler={this.handleOtpChange}
            />
          </div>
          <ActionWrapper>
            <div>
              <RenderButton
                type="resendOtp"
                onClick={this.generateOtp}
                label="Resend OTP"
                disabled={!this.state.isValidOtp}
              />
            </div>
            <div>
              <RenderButton
                type="primary"
                onClick={this.triggerLogin}
                label="LOGIN"
                disabled={!this.state.isValidOtp}
              />
            </div>
          </ActionWrapper>
        </div>
      );
      sendOtpButton = null;
    }
    return (
      <Row>
        <Column colWidth={6} offset={3}>
          <LoginWrapper>
            <div className="center-align">
              <TextInputField
                componentProps={componentProps}
                errorMessage={errorMessage}
                onChangeHandler={this.handleMobileChange}
              />
            </div>
            {otpBlock}
            {sendOtpButton}
          </LoginWrapper>
        </Column>
      </Row>
    );
  }
}

