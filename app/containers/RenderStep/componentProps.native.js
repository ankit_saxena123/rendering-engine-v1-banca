/*
This service contains the props passed to RenderStep's components
based upon their form_type
*/

import * as styleConfig from '../../configs/styleConfig';
import styleVars from '../../configs/styleVars';
import styledComponents from '../../configs/styledComponents';
import { appConst } from '../../configs/appConfig';
import * as common from '../../utilities/commonUtils';
import * as DocumentMap from '../../utilities/documentMap';
import { getParsedMeta } from './helper';

const keyBoardMap = {
  text: 'default',
  number: 'numeric',
};

// Render behaviour of components
// This return an object in compliance with Material UI setting of input text
export function getInputBasicRenderObj(renderData, meta) {
  const obj = {
    label: renderData.name,
    autoCapitalize: styleConfig.inputRenderSetting.capitalizeInput ? 'characters' : 'none',
  };

  if (meta.required || meta.required === 'true') {
    obj.label = styledComponents.getStyledRequiredLabel(obj.label);
  }

  return obj;
}
const getInputFieldProps = (data, elmId, defaultValue, formData) => {
  // basicRenderObj contains the coniguration for hintText, floatingLabelText etc etc
  // This is as per the app style setting for input texts
  // const basicRenderObj = getInputBasicRenderObj(data);
  const meta = getParsedMeta({...data.metaData, default: defaultValue}, formData),
        basicRenderObj = getInputBasicRenderObj(data, meta),
        formType = meta.form_type;

  const propObj = {
    ...basicRenderObj,
    value: typeof (meta.default) === 'number' ? meta.default.toString() : meta.default,
    disabled: meta.disabled,
    error: data.errorMessage,
    isCurrency: meta.isCurrency,
    keyboardType: keyBoardMap[formType],
    textOnly: meta.textOnly,
    secureTextEntry: meta.isSecure,
    noCopy: meta.noCopy,
    multiline: meta.multiline,
    prefix: meta.prefix,
    hintText: meta.hintText,
    accessibilityLabel: `ku-text-${elmId}`,

  };
  return propObj;
};

const getHiddenFieldProps = (data, elmId, defaultValue, formData) => {
  // basicRenderObj contains the coniguration for hintText, floatingLabelText etc etc
  // This is as per the app style setting for input texts
  const meta = getParsedMeta({...data.metaData, default: defaultValue}, formData),
    formType = meta.form_type;

  const propObj = {
    accessibilityLabel: `ku-hidden-${elmId}`,
    value: meta.default,
    editable: false,
  };
  return propObj;
};

const getCheckboxProps = (data, elmId, defaultValue, formData) => {
  // basicRenderObj contains the coniguration for hintText, floatingLabelText etc etc
  // This is as per the app style setting for input texts
  // TODO: drive the default settings from a config
  const meta = getParsedMeta({...data.metaData, default: defaultValue}, formData),
    formType = meta.form_type;

  meta.default = meta.default === 'false' || meta.default === '' ? false : meta.default;
  const propObj = {
    accessibilityLabel: `ku-checkbox-${elmId}`,
    checked: meta.default ? !!meta.default : false,
    editable: !meta.disabled,
    title: data.name,
  };
  return propObj;
};

function getRadioInitIndex(data, defaultValue) {
  let defaultIndex = -1;
  data.forEach((option, index) => {
    if (option.id === defaultValue) {
      defaultIndex = index;
    }
  });

  return defaultIndex;
}

const getRadioGroupProps = (data, elmId, defaultValue, formData) => {
  // basicRenderObj contains the coniguration for hintText, floatingLabelText etc etc
  // This is as per the app style setting for input texts
  // TODO: drive the default settings from a config
  // TODO: move size to styleConfig
  const meta = getParsedMeta({...data.metaData, default: defaultValue}, formData),
        basicRenderObj = getInputBasicRenderObj(data, meta);
  const propObj = {
    ...basicRenderObj,
    initial: getRadioInitIndex(data.enumValues, meta.default),
    editable: !meta.disabled,
    value: meta.default,
    type: meta.type,
    accessibilityLabel: `ku-radiogroup-${elmId}`,
  };
  return propObj;
};

const getDropdownProps = (data, elmId, defaultValue, formData) => {
  // TODO: drive the default settings from a config
  const meta = getParsedMeta({...data.metaData, default: defaultValue}, formData),
    label = data.name,
    formType = meta.form_type;

  const propObj = {
    label,
    accessibilityLabel: `ku-dropdown-${elmId}`,
    value: meta.default,
    disabled: meta.disabled,
    error: data.errorMessage,
  };
  if (meta.required || meta.required === 'true') {
    propObj.label = styledComponents.getStyledRequiredLabel(propObj.label);
  }
  return propObj;
};

const getDatepickerProps = (data, elmId, defaultValue, formData) => {
  // basicRenderObj contains the coniguration for hintText, floatingLabelText etc etc
  // This is as per the app style setting for input texts
  // TODO: drive the default settings from a config
  
  const meta = getParsedMeta({...data.metaData, default: defaultValue}, formData);
  const basicRenderObj = getInputBasicRenderObj(data, meta);
  const maxAge = meta.maxAge;
  const minAge = meta.minAge;
  const minDate = meta.minDate;
  const maxDate = meta.maxDate;
  const formType = meta.form_type;
  const validator = meta.validator;
  const dateStr = !!meta.default ? common.unformatDateStr(meta.default) : '';
  const isValidDate = common.isValidDate(dateStr);
  const propObj = {
    ...basicRenderObj,
    id: `ku-datepicker-${elmId}`,
    date: isValidDate ? new Date(dateStr) : null,
    value: !!meta.default ? meta.default : '',
    disabled: meta.disabled,
    formatDate: common.formatDefaultDate,
    error: data.errorMessage,
    isDobTextFieldEditable: meta.isDobTextFieldEditable || false,
  };
  if (validator.includes('date')) {
    if (maxDate !== undefined) {
      propObj.maximumDate = common.getExpiryDate(maxDate);
    }
    if (minDate !== undefined) {
      propObj.minimumDate = common.getExpiryDate(minDate);
    }
  } else {
    if (maxAge !== undefined) {
      propObj.minimumDate = common.getDateFromAge(maxAge);
    }
    if (minAge !== undefined) {
      propObj.maximumDate = common.getDateFromAge(minAge);
    }
  }
  return propObj;
};

const getSliderProps = (data, elmId, defaultValue, formData) => {
  // TODO: drive the default settings from a config

  const meta = getParsedMeta({...data.metaData, default: defaultValue}, formData),
    formType = meta.form_type;
  let isIrregular = meta.isStepIrregluar,
    valArr,
    step,
    sliderStepIndex;
  if (isIrregular) {
    valArr = meta.valArr;
    step = 1;
    meta.default = meta.default || valArr[0];
    sliderStepIndex = valArr.indexOf(meta.default);
  } else {
    meta.default = meta.default || meta.min * meta.step;
  }
  const propObj = {
    accessibilityLabel: `ku-slider-${elmId}`,
    value: typeof (meta.default) === 'string' ? parseInt(meta.default) : meta.default,
    disabled: meta.disabled,
    maximumValue: !isIrregular ? meta.max * meta.step : valArr.length - 1,
    minimumValue: !isIrregular ? meta.min * meta.step : 0,
    step: !isIrregular ? meta.step : 1,
    isCurrency: meta.isCurrency,
    valArr,
    isStepIrregluar: isIrregular,
    sliderStepIndex,
  };
  return propObj;
};

const getUploadProps = (data, elmId, defaultValue, formData) => {
  // TODO: drive the default settings from a config
  const meta = getParsedMeta({...data.metaData, default: defaultValue}, formData),
    formType = meta.form_type,
    docType = DocumentMap.getDocumentType(elmId);

  const propObj = {
    multiple: !!meta.isMultiple,
    value: meta.default,
    accept: meta.filetypes || 'application/pdf', // TODO: move this to a constant config
    // style: {height: 50}, //TODO: Build a styling object for this
    docSource: meta.source ? meta.source : 'fileManager',
    docType,
  };
  return propObj;
};

const getGeotrackingProps = (data, elmId, defaultValue, formData) => {
  const basicRenderObj = {
    label: data.name,
  };
  const meta = getParsedMeta({...data.metaData, default: defaultValue}, formData);
  const propObj = {
    ...basicRenderObj,
    value: meta.default,
    hintText: meta.hintText,
    accessibilityLabel: `ku-button-${elmId}`,
  };
  return propObj;
};

const getAutoCompleteProps = (data, elmId, defaultValue, formData) => {
  // basicRenderObj contains the coniguration for hintText, floatingLabelText etc etc
  // This is as per the app style setting for input texts
  const meta = getParsedMeta({...data.metaData, default: defaultValue}, formData),
        basicRenderObj = getInputBasicRenderObj(data, meta),
        formType = meta.form_type;

  const propObj = {
    ...basicRenderObj,
    value: meta.default,
    disabled: meta.disabled,
    error: data.errorMessage,
    allowKeyInput: meta.allowKeyInput,
    // TODO: move this errorText to a error-div inside the component like its done in checkbox
    keyboardType: keyBoardMap[formType],
    accessibilityLabel: `ku-autocomplete-${elmId}`,
  };
  return propObj;
};

const getDownloadProps = (data, elmId, defaultValue, formData) => {
  const meta = getParsedMeta({...data.metaData, default: defaultValue}, formData);

  const propObj = {
    url: meta.default,
    label: data.name,
    docType: meta.docType,
  };
  return propObj;
};

const getSocialFbProps = (data, elmId, defaultValue, formData) => {
  const meta = getParsedMeta({...data.metaData, default: defaultValue}, formData);

  const propObj = {
    value: meta.default,
  };
  return propObj;
};

const getAgreementProps = (data, elmId, defaultValue, formData) => {
  const meta = getParsedMeta({...data.metaData, default: defaultValue}, formData);
  const basicRenderObj = getInputBasicRenderObj(data, meta);
  const propObj = {
    ...basicRenderObj,
    redirectUrl: meta.redirectUrl,
    value: meta.default,
    error: data.errorMessage,
    docusignUrlKey: meta.docusignUrlKey,
  };

  return propObj;
};

const getHeadingProps = (data, elmId, defaultValue, formData) => {
  const meta = getParsedMeta({...data.metaData, default: defaultValue}, formData);
  const propObj = {
    label: data.name,
    type: meta.headingType,
    value: meta.default,
  };

  return propObj;
};

const getAccordionProps = (data, elmId, defaultValue) => {
  const meta = data.metaData;
  const propObj = {
    label: data.name,
    imageUrl: meta.imageUrl,
  };

  return propObj;
};

const getRadioAccordionProps = (data, elmId, defaultValue) => {
  const meta = data.metaData;
  const propObj = {
    label: data.name,
    valueHolder: meta.valueHolder,
    imageUrl: meta.imageUrl,
  };

  return propObj;
};

const getSubJourneyTriggerProps = (data, elmId, defaultValue) => {
  const meta = data.metaData;
  const propObj = {
    label: data.name,
  };

  return propObj;
};

const getIconProps = (data, elmId, defaultValue) => {
  const meta = data.metaData;
  const propObj = {
    type: meta.type,
    //disabled: isReadMode
  };

  return propObj;
};

const getExpressionProps = (data, elmId, defaultValue) => {
  const meta = data.metaData;
  const propObj = {

  };

  return propObj;
}

export default {
  text: getInputFieldProps,
  rest: getInputFieldProps,
  number: getInputFieldProps,
  hidden: getHiddenFieldProps,
  checkbox: getCheckboxProps,
  radio: getRadioGroupProps,
  dropdown: getDropdownProps,
  'multiselect-dropdown': getDropdownProps,
  'rest-dropdown': getDropdownProps,
  datepicker: getDatepickerProps,
  slider: getSliderProps,
  upload: getUploadProps,
  autocomplete: getAutoCompleteProps,
  download: getDownloadProps,
  elasticAutocomplete: getAutoCompleteProps,
  'social-facebook': getSocialFbProps,
  docuSign: getAgreementProps,
  heading: getHeadingProps,
  accordion: getAccordionProps,
  radioAccordion: getRadioAccordionProps,
  subJourney: getSubJourneyTriggerProps,
  locator: getGeotrackingProps,
  icon: getIconProps,
  expression: getExpressionProps,
};
