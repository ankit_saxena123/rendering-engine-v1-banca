// import { take, call, put, select } from 'redux-saga/effects';


import { call, put, select, take, takeLatest, takeEvery } from 'redux-saga/effects';

import * as userUtils from '../../utilities/userUtils';
import * as C from '../../containers/AppDetails/constants';

import request from '../../utils/request';
import * as NavigationService from '../../utilities/NavigationService';
import * as requestUtils from '../../utilities/requestUtils';
import * as sessionUtils from '../../utilities/sessionUtils';
import { reportLog } from './sagaHelper';
import { pushIntoInProcessArray, removeFromInProcessArray, isQueryInProcess } from './queryHelper';
import { changeLanguage, getAllLanguages, fallbackLanguage } from '../../utilities/localization';
import * as appActions from './actions';
import * as renderActions from '../RenderJourney/actions';
import * as inactivityTracker from '../../utilities/inactivityTracker';

import * as appDetailsSelector from '../../containers/AppDetails/selectors';
import { userConfig } from '../../configs/appConfig';

const currentActiveCalls = [];

export default function* defaultSaga() {
  const watcherGetNetworkSaga = yield takeEvery(C.GET_REQUEST, networkSaga);
  const watcherPostNetworkSaga = yield takeEvery(C.POST_REQUEST, networkSaga);
  const watcherDeleteNetworkSaga = yield takeEvery(C.DELETE_REQUEST, networkSaga);

  // yield take(LOCATION_CHANGE);

  // Using SAGAs as service to perform logic and then dispatch actions
  const watcherSetUserDetails = yield takeEvery(C.SET_USER_DETAILS, setUserDetails);
  const watcherSetUserLanguage = yield takeEvery(C.SET_USER_LANGUAGE, setUserLanguage);
  const watcherNavigateToScreen = yield takeEvery(C.NAVIGATE_TO_SCREEN, navigateToScreen);
  yield takeEvery(C.DUMP_LOG, dumpLog);
  yield takeEvery(C.LOGOUT, logoutUser);
}

export function* networkSaga(action) {
  // If it is being run for previewing Forms (In UI Editor) - Never issue any request
  if (userConfig.runAsPreviewForm) {
    return;
  }
  let response = {};
  const actionData = action.data.data;
  actionData.userDetails = yield select(appDetailsSelector.makeSelectUserDetails());
  let requestData;// = requestUtils.formGetRequestObject
  if (action.type === C.GET_REQUEST) {
    requestData = requestUtils.formGetRequestObject(action.data.key, actionData);
  }
  if (action.type === C.POST_REQUEST) {
    requestData = requestUtils.formPostRequestObject(action.data.key, actionData);
  }
  if (action.type === C.DELETE_REQUEST) {
    requestData = requestUtils.formDeleteRequestObject(action.data.key, actionData);
  }
  /* FORMAT of requestData
    {
      type: SOMETHING_SOMETHING,
      data: {
        url: ,
        options: {
          method: 'POST/GET',
          params: `PAYLOAD`,
          successCb: `Fn`,
          headers: `HEADERS`
        }
      }
    }
  */

  // Check against the current existing "In Process Query Array"
  // If the same query with URL and options exist then do nothing (ignore the second call to API)
  // TODO - **** push into array 
  // if (isQueryInProcess(requestData.url, requestData.options)) {
  //   return;
  // } else {
  //   pushIntoInProcessArray(requestData.url, requestData.options);
  // }
  // show loader
  if (!actionData.loader) {
    // TODO: handle this in loader component
    let longReqObj = requestUtils.getLongReqObj(actionData.screenId);
    if (actionData.isLongReq && actionData.loaderMessage) {
      longReqObj = {
        isLongReq: actionData.isLongReq,
        message: actionData.loaderMessage,
      };
    }
    currentActiveCalls.push(requestData.url);
    yield put(appActions.updateLoadingState({
      isVisible: true,
      ...longReqObj,
    }));
  }
  // reset inactivity tracker
  inactivityTracker.resetInactivity();
  response = yield call(request, requestData.url, requestData.options);
  if (response) {
    if (response.status && response.status === 401) {
      // issue a refreshToken request
      const refreshTokenData = requestUtils.buildRefreshTokenObj('refreshToken', { params: { refresh_token: requestData.options.refreshToken } });
      const response_refresh = yield call(request, refreshTokenData.url, refreshTokenData.options);

      if (response_refresh) {
        if (response_refresh.status && response_refresh.status === 200) {
          const userDetailsObj = yield select(appDetailsSelector.makeSelectUserDetails());
          const updatedUserDetailsObject = {
            ...userDetailsObj,
            bpm_token: response_refresh.data.access_token,
            refreshToken: response_refresh.data.refresh_token,
          };
          yield put(appActions.setUserDetails(updatedUserDetailsObject));
          // not making new object no, reprocessing the request through itself instead.
          // yield put(appActions.postRequest(action.data))
          // const updatedRequestOptions = requestUtils.updateRequestObjectOptions(requestData.options, updatedUserDetailsObject);
          // yield call(request, requestData.url, updatedRequestOptions);
          yield put(action);
        } else {
          const userDetailsObj = yield select(appDetailsSelector.makeSelectUserDetails());
          yield put(appActions.setAppError({ show: true, customMessage: 'Session time out! Please Login again.' }));
          yield put(appActions.setUserDetails({
            ...userDetailsObj,
            bpm_token: null,
          }));
        }
      }
      if (requestData.options.errorCb) {
        yield call(requestData.options.errorCb);
      }
    } else if (response.status && response.status !== 200 && response.status !== 'success' && response.status !== 'OK' && response.status !== true && response.status !== 204) {
      if (requestData.options.errorCb) {
        yield call(requestData.options.errorCb);
      } else {
        yield put(appActions.setAppError({ show: true }));
      }
    } else {
      yield put(appActions.setAppError({ show: false }));
      yield call(requestData.options.successCb, response);
      const { successAction } = actionData;
      if (successAction) {
        yield put(successAction(response));
      }
    }
  } else if (requestData.options.errorCb) {
    yield call(requestData.options.errorCb);
  }
  // remove from the inProcess array 
  // removeFromInProcessArray(requestData.url, requestData.options);
  // Loader handling
  const targetIndex = currentActiveCalls.indexOf(requestData.url);
  if (targetIndex !== -1) {
    currentActiveCalls.splice(targetIndex, 1);
  }
  // hide loader
  if (!currentActiveCalls.length) {
    yield put(appActions.updateLoadingState({
      isVisible: false,
      isLongReq: false,
      message: '',
    }));
  }
}


export function* setUserDetails(action) {
  // Set details in sessionStorage
  yield userUtils.setUserDetails(action.data);
}

export function* setUserLanguage(action) {
  let language = action.data;
  const results = yield call(() => new Promise((resolve) => {
    changeLanguage(action.data, (result) => {
      if (result) {
        resolve(results);
      } else {
        resolve(results);
      }
      return result;
    });
  }));
  const allLanguages = getAllLanguages();
  if (!allLanguages.includes(action.data)) {
    language = fallbackLanguage;
  }
  yield put(appActions.updateUserLanguage(language));
  sessionUtils.setSessionKey('userLanguage', language);
}

export function* navigateToScreen(action) {
  if (NavigationService && NavigationService.navigate) {
    NavigationService.navigate(action.screen, action.params);
  }
}

export function* dumpLog(action) {
  reportLog(action.data);
  console.log('action: ', action);
}

export function* clearJourneyData() {
  yield put(renderActions.clearJourneyData());
}

export function* logoutUser() {
  const userDetails = yield call(userUtils.logout);
  yield put(appActions.setUserDetails(userDetails));
  yield put(renderActions.clearJourneyData());
  yield put(appActions.navigateToSCreen('Login'));
}
