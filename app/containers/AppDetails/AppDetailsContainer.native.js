/**
 *
 * AppDetails Native
 *
 */

import React from 'react';
import { Platform, View, KeyboardAvoidingView } from 'react-native';
import AppDetailsAbstract from './AppDetailsAbstract';
import { strings } from '../../../locales/i18n';
import * as styles from './AppDetailsStyles.native';
import { checkSupport } from '../../utilities/native/touchIDService.native';
import { appConst, userConfig } from '../../configs/appConfig';
import { colors, brand } from '../../configs/styleVars';
import Login from '../Login/';
import RenderOtp from '../RenderOtp/';
import RenderJourney from '../RenderJourney/';
import AppHeader from '../../components/AppHeader/';
import Loader from '../../components/Loader';
import BottomNavBar from '../../components/RenderStaticComponents/BottomNavBar';
import RenderPopup from '../../components/RenderPopup';
import RenderAppError from '../../components/RenderAppError';
import MainNavigation from '../../navigation/MainNavigation.native';

console.disableYellowBox = true;
export default class AppDetailsContainer extends AppDetailsAbstract { // eslint-disable-line react/prefer-stateless-function

  init = () => console.log('Init called');

  checkTouchIDStatus = async () => {
    const supportStatus = await checkSupport();
    this.setState({ useTouchID: supportStatus });
  }

  toggleTouchID = (value) => {
    this.setState({ setTouchID: value });
  }

  render() {
    if (true || this.state.initiateScreen) {
      const scrollBehave = {};
      if (Platform.OS === 'ios') {
        scrollBehave.behavior = 'padding';
      }
      let appErrorBlock = null;
      if (this.props.showAppError.show) {
        appErrorBlock = (
          <RenderAppError
            isLoggedin={this.state.mpinConfirmed}
            toggleMpin={this.toggleMpin}
            setAppError={this.props.setAppError}
            {...this.props}
          />
        );
      }

      return (
        <KeyboardAvoidingView
          style={{ flex: 1 }}
          {...scrollBehave}
        >
          <View {...styles.wrapperStyles}>
            <Loader loaderStatus={this.props.loadingState} />
            <RenderPopup
              {...this.props.popupData}
            />
            <MainNavigation
              userDetails={this.props.userDetails}
              userLanguage={this.props.userLanguage}
              dashboardData={this.props.dashboardData}
              currentScreen={this.props.currentScreen}
              logout={this.props.logout}
            />
          </View>
          {React.Children.toArray(this.props.children)}
          {appErrorBlock}
        </KeyboardAvoidingView>
      );
    }
    return null;
  }
}
