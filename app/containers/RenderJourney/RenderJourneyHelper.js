import * as validationFns from '../../utilities/validations/validationFns';

export function getCleanedFormData(formData, renderData) {
  const currentFormData = { ...formData };
  const formDataFields = Object.keys(currentFormData);
  formDataFields.forEach((formFieldId) => {
    const formFieldValue = currentFormData[formFieldId];
    currentFormData[formFieldId] = typeof (formFieldValue) === 'string' ? formFieldValue.trim() : formFieldValue;
    if (renderData[formFieldId].isHidden) {
        // If a field is hidden - skip the validation
        // TODO: Once the backend implementation - remove the field wholetogether
      currentFormData[formFieldId] = '';
    }
    /* 
      // If a field is dummyField 
      //    -> it is only used as a reference to display other composite component (editableTable or cloning component)
      // Or if the field Un-writable
      // It shouldn't be submitted
    */
    if (renderData[formFieldId] && !renderData[formFieldId].isWritable || (renderData[formFieldId].metaData && renderData[formFieldId].metaData.dummyField)) {
      delete currentFormData[formFieldId];
    }
  });
  return currentFormData;
}

const getUnitValidated = (value, fieldId, renderData, formData) => {
  if (renderData[fieldId]) {
    const validationObj = validationFns.validateElm(renderData[fieldId], value, formData);
    return validationObj;
  } else {
    // If the renderData for a particular field does not exist 
    // it means its an extra data sent for the record from the BackEnd itself - no reference to validate it from
    return {
      isValid: true,
      errorMessage: '',
    }
  }
    
};

const checkMultiInstanceValidation = (value, key, renderData, formData) => {
  // MultiInstance value would an stringified array of objects
  let errorData = {};
  if (value) {
    const records = JSON.parse(value);
    let allValid = true;
    let allErrors = [];
    Object.keys(records).forEach((recordIndex) => {
      const recordData = records[recordIndex];
      const instanceKeys = renderData[key].metaData.fields.map(e => e.fieldId);
      instanceKeys.forEach((fieldKey, index) => {
        let { isValid, errorMessage } = getUnitValidated(recordData[fieldKey], fieldKey, renderData, {...formData, ...recordData});
        allValid = allValid && isValid;
        !isValid && allErrors.push('Row - '+(recordIndex+1)+': '+errorMessage);
      });
    });

    return {
      isValid: allValid,
      errorMessage: allErrors,
      // to trigger the internal validation for this component and display the individual error messages
      toggleInternalValidation: !renderData[key].toggleInternalValidation,
    }
  } else {
    return {
      isValid: true,
      errorMessage: '',
      toggleInternalValidation: !renderData[key].toggleInternalValidation,
    }
  }

}

export function getValidatedObj(value, elmId, renderData, formData) {
  let validObj;
  // if editableTable or cloning => perform validation for each of record's each element and set the flag aacordingly
  if (renderData[elmId].metaData.form_type === 'editableTable' ||renderData[elmId].metaData.form_type === 'cloning') {
    validObj = checkMultiInstanceValidation(value, elmId, renderData, formData);
  } else {
    validObj = validationFns.validateElm(renderData[elmId], value, formData);
  }
  const currentRenderData = { ...renderData };
  // Set errorMessage and invalid flag
  currentRenderData[elmId] = {...currentRenderData[elmId], ...validObj};
  return currentRenderData[elmId];
}

export function getValidatedRenderData(formData, renderData, fieldOrder) {
  const currentRenderData = { ...renderData };
  fieldOrder.forEach((key) => {
    if (renderData[key]) {
      if (renderData[key].isHidden) {
        // If a field is hidden - skip the validation
        currentRenderData[key].isValid = true;
      } else {
        currentRenderData[key] = getValidatedObj(formData[key], key, renderData, formData);
      }
    } else {
      window.logger.error('META', `Couldn't find ${key} in render data`, { renderData, fieldOrder });
    }
  });
  return currentRenderData;
}

export function getBackTaskScreenId(screenKey, completedTasks) {
  // const completedTasks = self.props.renderjourney.stepperData && self.props.renderjourney.stepperData.backTaskList;
  let backScreenKey = null;
  if (screenKey) {
    const isValidKey = Array.isArray(completedTasks) && completedTasks.some((taskObj) => taskObj && taskObj.taskDefinitionKey === screenKey);
    backScreenKey = isValidKey && screenKey;
  } else {
    backScreenKey = Array.isArray(completedTasks) && completedTasks[completedTasks.length - 1].taskDefinitionKey;
  }
  return backScreenKey;
}
