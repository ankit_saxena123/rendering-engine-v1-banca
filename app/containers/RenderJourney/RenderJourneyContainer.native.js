/**
 *
 * RenderJourney
 *
 */

import React from 'react';
import { View, ScrollView, Keyboard, Dimensions, Animated } from 'react-native';
import { Button } from 'react-native-elements';

import { Row, Column as Col, Grid } from 'react-native-responsive-grid';

import RenderStep from '../RenderStep/';
import { appConst, layoutConfig } from '../../configs/appConfig';
import RenderHeader from '../../components/RenderHeader/';
import RenderStepper from '../../components/RenderStepper/';
import RenderSideNav from '../../components/RenderSideNav/';
import RenderButton from '../../components/RenderButton/';
// import AppHeader from '../../components/AppHeader';

import RenderSubJourney from '../RenderSubJourney';
import * as renderService from '../../utilities/renderDataService';
import RenderPrebuiltStatic from '../../components/RenderPrebuiltStatic/';
import RenderJourneyAbstract from './RenderJourneyAbstract';
import RenderCancelJourney from '../../components/RenderCancelJourney';
import * as JourneyStyles from './JourneyStyles.native';
import { strings } from '../../../locales/i18n';
import { AppLogo } from '../../components/RenderStaticComponents/';
import { verticalScale } from '../../configs/scalingUtils';

const SCREEN_HEIGHT = Dimensions.get('window').height;
export default class RenderJourneyContainer extends RenderJourneyAbstract { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.state = {
      scrollEnabled: true,
      continueButton: strings('renderJourney.continueButtonText'),
      atBottom: false,
      contentHeight: 0,
      hideBtnBlock: false,
    };
    this.initial = true;
  }

  componentWillMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', () => this.toggleBtnBlockView(true));
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', () => this.toggleBtnBlockView(false));
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  toggleBtnBlockView = (value) => this.setState({ hideBtnBlock: value });

    // to allow scrollable element within ScrollView
  toggleScroll = (scrollObj) => {
    this.setState({ ...scrollObj });
  }

  scrollToTop = () => {
    if (this.refs._scrollView) {
      this.refs._scrollView.scrollTo({ x: 0, y: 0, animated: true });
    }
  }

  isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
    const paddingToBottom = 20;
    return layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom;
  };

  onContentSizeChange = (contentWidth, contentHeight) => {
    this.setState({ contentHeight });
    // verticalScale(88) - header size
    // TODO: move header size to app constants
    if (contentHeight > SCREEN_HEIGHT - verticalScale(88)) {
      this.setState({ continueButton: '>' });
    } else {
      this.setState({ continueButton: strings('renderJourney.continueButtonText') });
    }
  }

  enableContinueButton = () => {
    this.setState({ continueButton: strings('renderJourney.continueButtonText'), atBottom: true });
  };

  onScroll =(event) => {
    if (this.isCloseToBottom(event.nativeEvent)) {
      this.enableContinueButton();
    }
  }

  onScrollSubmit = (_scrollToBottomY) => {
    if (this.state.continueButton === '>') {
      this.refs._scrollView.scrollTo(_scrollToBottomY);
    }
    Keyboard.dismiss();
    if (this.state.continueButton === strings('renderJourney.continueButtonText')) {
      this.submitStep();
    }
  }

  render() {
    let renderStep,
      headerBlock,
      stepperBlock = null,
      sideNavBlock = null,
      nextButton = null,
      submitButton = null,
      btnBlock = null,
      btnStyle = {},
      _scrollToBottomY = SCREEN_HEIGHT,
      cancelBlock = null,
        // TODO: merge it with the API's categories and subcategories
      categoryLabels,
      currentActiveCategory,
      defaultScreenList;
      // console.log('FormData',this.props.renderjourney.formData);
      // console.log('RenderData',this.props.renderjourney.renderData);
    const stepperData = this.props.renderjourney.stepperData;
    if (this.props.renderjourney && this.props.renderjourney.refreshFlag
        && this.props.renderjourney.stepData
        && this.props.renderjourney.screenId
        && this.props.renderjourney.fieldOrder
        && this.props.renderjourney.formData) {
      // reset initial condition
      this.initial = false;
      if (stepperData && typeof (stepperData) === 'object' && stepperData.category) {
        categoryLabels = stepperData.category;
        currentActiveCategory = stepperData.currentActiveCategory;
        defaultScreenList = stepperData.defaultScreenId;
        if (categoryLabels && categoryLabels.length) {
        // This is Stepper for the steps
          stepperBlock = (<RenderStepper
            labels={categoryLabels}
            currentActiveStep={currentActiveCategory}
            defaultScreenList={defaultScreenList}
            backTrigger={this.backTrigger}
          />);

          sideNavBlock = (<RenderSideNav
            labels={categoryLabels}
            currentActiveStep={currentActiveCategory}
            defaultScreenList={defaultScreenList}
            backTrigger={this.backTrigger}
          />);
        }
      }
      cancelBlock = (<RenderCancelJourney
        setPopupData={this.props.setPopupData}
        cancelJourney={this.cancelApplication}
      />);

        // This is step's header - container the screen heading with counter
      const applicationId = this.props.renderjourney.stepData.data.data.applicationId;
      if (layoutConfig.includeHeader && stepperData) {
        headerBlock = (<RenderHeader stepData={stepperData} applicationId={applicationId} />);
      }
      const isLastStep = renderService.getLastStepFlag(this.props.renderjourney.screenId);
      const isSingleComponent = renderService.getSingleComponentFlag(this.props.renderjourney.screenId);
      const isPrebuiltStaticComponent = renderService.getPrebuiltStaticFlag(this.props.renderjourney.screenId);
        // If its not a single custom component or a prebuilt static (html) component
        // Then use the renderStep which uses basic unit components based on form_types
        // Else use the prebuilt/single components
      if (isSingleComponent) {
        const ScreenComponent = renderService.getComponentByScreenId(this.props.renderjourney.screenId);
        renderStep = (<ScreenComponent
          screenId={this.props.renderjourney.screenId}
          renderData={this.props.renderjourney.renderData}
          stepperData={stepperData}
          fieldOrder={this.props.renderjourney.fieldOrder}
          formData={this.props.renderjourney.formData}
          validateAndUpdateRender={this.validateAndUpdateRender}
          updateFormData={this.updateFormData}
          submitStep={this.submitStep}
          toggleScroll={this.toggleScroll}
        />);
      } else if (isPrebuiltStaticComponent) {
        renderStep = (<RenderPrebuiltStatic
          screenId={this.props.renderjourney.screenId}
          renderData={this.props.renderjourney.renderData}
          fieldOrder={this.props.renderjourney.fieldOrder}
          formData={this.props.renderjourney.formData}
          validateAndUpdateRender={this.validateAndUpdateRender}
          updateFormData={this.updateFormData}
        />);
      } else {
          // TODO: All the below props should be a direct substate call for render journey
          // Example: in mapStateToProps
          // formData: makeSelectFormData()
          // And to be used below as: formData:{this.props.formData}
        renderStep = (<RenderStep
          screenId={this.props.renderjourney.screenId}
          renderData={this.props.renderjourney.renderData}
          fieldOrder={this.props.renderjourney.fieldOrder}
          formData={this.props.renderjourney.formData}
          validateAndUpdateRender={this.validateAndUpdateRender}
          updateFormData={this.updateFormData}
          setRenderData={this.props.setRenderData}
          toggleScroll={this.toggleScroll}/>);
      }
      btnStyle = JourneyStyles.getButtonStyle('next');

      nextButton = (<Button
        onPress={() => { this.onScrollSubmit(_scrollToBottomY); }}
        text={this.state.continueButton}
        {...btnStyle}
      />);

      const showBackButton = (this.props.renderjourney.completedTasks && this.props.renderjourney.completedTasks.length);
      btnStyle = JourneyStyles.getButtonStyle('back', !showBackButton);
      const backButton = showBackButton ? (<RenderButton
        onPress={() => { Keyboard.dismiss(); this.backTrigger(); }}
        type={'secondary'}
        text="Back"
        style={{ ...btnStyle }}
      />) : null;

      btnStyle = JourneyStyles.getButtonStyle('next');
      submitButton = (<Button
        text={strings('renderJourney.submitButtonText')}
        style={{ ...btnStyle }}
      />);

      if (!isLastStep) {
        btnBlock = (<View {...JourneyStyles.btnWrapper}>
          {backButton}
          {nextButton}
        </View>);
      } else {
        btnBlock = (<View {...JourneyStyles.btnWrapper}>
        </View>);
      }
    }

    if (this.props.renderjourney.subJourneyId) {
      renderStep = (<RenderSubJourney
        triggerInitiate={this.triggerInitiate}
        screenId={this.props.renderjourney.screenId}
        renderData={this.props.renderjourney.renderData[this.props.renderjourney.subJourneyId]}
        subJourneyId={this.props.renderjourney.subJourneyId}
        fieldOrder={this.props.renderjourney.fieldOrder}
        formData={this.props.renderjourney.formData}
        validateAndUpdateRender={this.validateAndUpdateRender}
        updateFormData={this.updateFormData}
        toggleScroll={this.toggleScroll}
      />);

      return (<View {...JourneyStyles.mainWrapperStyle}>
        {renderStep}
      </View>);
    }
    if (this.initial) {
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignContent: 'center' }} >
          <AppLogo
            height={40}
            style={{ alignSelf: 'center' }}
          />
        </View>
      );
    }

    if (stepperData && typeof (stepperData) === 'object' && stepperData.fullScreen) {
      return (<View {...JourneyStyles.mainWrapperStyle}>
        {renderStep}
      </View>);
    }

    const isSideNavVisible = sideNavBlock && layoutConfig.isSideNavShown;
    const isStepperVisible = stepperBlock && layoutConfig.isStepperShown;

    const sideNavWrapper = isSideNavVisible ? (<Col size={20} style={{ flex: 1, backgroundColor: '#FFF', marginRight: 5 }}>
      {sideNavBlock}
    </Col>) : null;
    const stepperBlockWrapper = isStepperVisible ? (<View style={JourneyStyles.stepperWrapper}>
      {stepperBlock}
    </View>) : null;
    const journeyWrapperColSize = isSideNavVisible ? 80 : 100;
    // let appHeaderBlock = (<AppHeader type='appHeader' isLoggedin logout={this.props.screenProps.logout}/>);
    return (
      <View {...JourneyStyles.mainWrapperStyle}>
        {/* {stepperBlockWrapper} */}
        {/* {appHeaderBlock} */}
        <Row size={100} style={{ flex: 1, paddingTop: 5 }}>
          {sideNavWrapper}
          <Col size={journeyWrapperColSize} >
            <View
              style={{ flex: 1, backgroundColor: '#FFFFFF' }}
            >
              <ScrollView
                keyboardShouldPersistTaps="always"
                onScroll={this.onScroll}
                ref="_scrollView"
                onContentSizeChange={this.onContentSizeChange}
                onScrollBeginDrag={this.resetInactivity}
                onStartShouldSetResponderCapture={() => false}
                scrollEnabled={this.state.scrollEnabled}
                {...JourneyStyles.stepWrapperStyle}
                automaticallyAdjustContentInsets={false}
              >
                {stepperBlockWrapper}
                {headerBlock}
                <View {...JourneyStyles.formWrapper}>
                  {renderStep}
                </View>
                {this.state.hideBtnBlock && isStepperVisible ? <View style={JourneyStyles.btnBlockInScrollStyles}>{btnBlock}</View> : null}
              </ScrollView>
            </View>

            {this.state.hideBtnBlock && isStepperVisible ? null : btnBlock}
          </Col>
        </Row>
      </View>
    );
  }
}
