/*
 *
 * RenderSubJourney reducer
 *
 */

import { fromJS } from 'immutable';
import * as C from './constants';

const initialState = fromJS({
  subJourneyId: null,
});

function renderSubJourneyReducer(state = initialState, action) {
  switch (action.type) {
    case C.DEFAULT_ACTION:
      return state;
    case C.SET_SUBJOURNEY_ID:
      return state
        .set('subJourneyId', action.data);
    default:
      return state;
  }
}

export default renderSubJourneyReducer;
