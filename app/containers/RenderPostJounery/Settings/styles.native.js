import { fonts,
    sizes,
    lineHeight,
    colors } from '../../../configs/styleVars';

export const wrapperStyle = {
  backgroundColor: colors.white,
  paddingHorizontal: sizes.appPaddingHorizontal,
  flex: 1,
  justifyContent: 'space-between',
};

export const titleStyle = {
  color: colors.primaryBGColor,
  fontSize: fonts.h2,
  lineHeight: lineHeight.h2,
  paddingVertical: 10,
  paddingTop: sizes.headerPadding,
  ...fonts.getFontFamilyWeight('bold'),
};


export const tileStye = {
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
  paddingVertical: 10,
  borderBottomWidth: 1,
  borderBottomColor: colors.underlineColor,
};
