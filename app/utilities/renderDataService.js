/*
  This service mainly contains all the methods/functions/utilities to be used in rendering the step
  To build all the useful data objects to render all the relative components
*/
import uniq from 'lodash/uniq';
import * as componentMap from './componentMap';
import { currentLocale, getTranslation, getRestProps } from './localization';
import * as common from './commonUtils';

let defaultFormData = {};


export const getDefaultFormData = (fieldID) => defaultFormData && defaultFormData[fieldID];

export const getFieldOrder = (renderData) => {
  const screenId = renderData.screenInfo;
  const fieldIds = Object.keys(renderData);
  fieldIds.sort((a, b) => {
    if (renderData[a].metaData.order < renderData[b].metaData.order) {
      return -1;
    }
    if (renderData[a].metaData.order > renderData[b].metaData.order) {
      return 1;
    }
    return 0;
  });
  return fieldIds;
};

export const checkInvalidDataValidation = (meta) => {
  switch (meta.form_type) {
    case 'autocomplete':
    case 'elasticAutocomplete':
      // allowKeyInput: false => means we dont want user to enter its own input and get away 
      // => only the selection from the populated options is valid 
      return !meta.allowKeyInput;

    case 'upload':
      return true;

    default:
      return false;
  }
};

// Build the rendering data consisting the meta info for each field in the step
export const buildRenderData = (stepData) => {
  const screenId = stepData.screenInfo;
  const fieldIds = Object.keys(stepData.data[screenId]);
  const renderDataDummy = stepData.data[screenId];

  // Convert meta string to metaData JSON for each field
  fieldIds.forEach((key) => {
    renderDataDummy[key].metaData = renderDataDummy[key].params && renderDataDummy[key].params.meta;

      // Check for 'display-on-selection' key
      // To set the isHidden flag to render or not render this field
    const meta = renderDataDummy[key].metaData;
    if (meta['display-on-selection']) {
      const displayConfig = meta['display-on-selection'];
      let unionFormIds = [];
      Object.keys(displayConfig).forEach((value) => {
        const dependentFields = displayConfig[value];
        unionFormIds = unionFormIds.concat(dependentFields);
      });
      meta.dependentFields = uniq(unionFormIds);
    }
    // If the field is a dummy field ->
    // not being used anywhere but exist for reference of other fields (like in case of editable table)
    if (meta.dummyField) {
      renderDataDummy[key].isHidden = true;
    }
    // for valid autocomplete dropdown selection
    if (checkInvalidDataValidation(meta)) {
      if (meta.validator) {
        const validators = JSON.parse(meta.validator);
        validators.push('isValidData');
        meta.validator = JSON.stringify(validators);
      } else {
        meta.validator = '[]';
      }
    }

    // for label localization
    if (meta && meta.lang) {
    //  get selected app language
      renderDataDummy[key].name = meta.lang[currentLocale()];
    }
    if (meta && meta.infoData) {
      renderDataDummy[key].metaData.infoData = getTranslation(meta.infoData);
    }

    if (meta && meta.suffix) {
      renderDataDummy[key].metaData.suffix = getTranslation(meta.suffix);
    }

    const enumValues = renderDataDummy[key].enumValues && [...renderDataDummy[key].enumValues];
    if (enumValues && enumValues.length) {
      renderDataDummy[key].enumValues = enumValues.map((value) => ({
        ...value,
        name: getTranslation(value.name),
        ...getRestProps(value.name),
      }));
    }

    if (meta && meta.form_type === 'popup') {
      renderDataDummy[key] = translatePopupData(renderDataDummy[key]);
    }
  });

  return renderDataDummy;
};

const translatePopupData = (data) => {
  const dummyData = { ...data };
  const meta = dummyData.metaData;
  const translateArr = ['bodyText', 'headerText'];
  translateArr.forEach((prop) => {
    meta[prop] = getTranslation(meta[prop]);
  });
  if (meta.actionButtons) {
    meta.actionButtons.forEach((btnObj, index) => {
      meta.actionButtons[index] = {
        ...btnObj,
        label: getTranslation(btnObj.label),
      };
    });
  }
  return data;
};

// Decide whether its an element render or a single componenet render
const singleComponentSteps = ['emiCalculator', 'loanRequirements', 'loanRequirementsCopy', 'loanoffer', 'statusScreen', 'rejectMessageRetry', 'rejectionMessage', 'congratulationScreen', 'congratsScreen', 'editProfile'];

export const getSingleComponentFlag = (screenId) => {
  const id = screenId.split('-')[0];
  return (singleComponentSteps.indexOf(id) > -1);
};

// A prebuilt static component flag
// Componenet with this flag is a straight html string from BE
export const getPrebuiltStaticFlag = (screenId) => {
  let tempArr = screenId.split('-'),
    prebuiltComponent;
  if (tempArr.length > 1 && tempArr[1] === 'prebuilt') {
    prebuiltComponent = true;
  } else {
    prebuiltComponent = false;
  }
  return prebuiltComponent;
};

// Determine whether this is the last step in the journey
export const getLastStepFlag = (screenId) => {
  let lastStepFlag;
  if (screenId) {
      // split the screen id with delimiter '-' . If the second element in array is last
    const splitArr = screenId.split('-');
    if (splitArr.length > 1 && (splitArr[splitArr.length - 1] == 'last')) {
      lastStepFlag = true;
    } else {
      lastStepFlag = false;
    }
  }
  return lastStepFlag;
};


// Build default form data to be submitted through the steps
//
export const buildFormData = (renderData, fieldOrder) => {
  const stepFormData = {};
  if (renderData && fieldOrder) {
    fieldOrder.forEach((fieldId) => {
      let defaultValue = renderData[fieldId].value;
      if (!defaultValue && defaultValue !== 0) {
        defaultValue = renderData[fieldId].metaData && renderData[fieldId].metaData.default;
      }
      if (renderData[fieldId].value === false) {
        defaultValue = false;
      }
      const enumConfig = renderData[fieldId].metaData.enumField;
      if (renderData[fieldId].metaData.enumField && typeof (renderData[fieldId].metaData.enumField) === 'object') {
        const targetFieldId = enumConfig.targetFieldId;
        if (targetFieldId && renderData[targetFieldId]) {
          const currentFieldValue = renderData[targetFieldId].value || (renderData[targetFieldId].metaData && renderData[targetFieldId].metaData.default);

          const defaultValueObj = enumConfig.values && enumConfig.values[currentFieldValue];
          if (defaultValueObj) {
            defaultValue = getTranslation(defaultValueObj);
          }
        }
      }

      stepFormData[fieldId] = (defaultValue === false || defaultValue || defaultValue === 0) ? defaultValue : '';
      if (renderData[fieldId].metaData.form_type === 'slider' && !renderData[fieldId].value) {
        const meta = renderData[fieldId].metaData;
        let isIrregular = meta.isStepIrregluar,
          valArr;
        if (isIrregular) {
          valArr = meta.valArr;
          defaultValue = defaultValue || valArr[0];
        } else {
          defaultValue = defaultValue ? Number(defaultValue) : meta.min * meta.step;
        }
        stepFormData[fieldId] = defaultValue;
      }
      if (renderData[fieldId].metaData.form_type === 'editableTable') {
        if (typeof stepFormData[fieldId] === 'object') {
          stepFormData[fieldId] = JSON.stringify(stepFormData[fieldId]);
        }
      }
      if (renderData[fieldId].metaData.form_type === 'checkbox') {
        if (defaultValue === 'false') {
          stepFormData[fieldId] = false;
        } else if (defaultValue === 'true') {
          stepFormData[fieldId] = true;
        }
      }
    });
  }
  defaultFormData = { ...stepFormData };
  return stepFormData;
};

export const isConditionalPropExist = (renderData, elmId, prop) => {
  const meta = renderData[elmId] && renderData[elmId].metaData;
  return meta && meta.conditionalMeta && meta.conditionalMeta[prop] && meta.conditionalMeta[prop].trim();
}

export const performConditionalHideOperation = (renderData, formData, elmId) => {
  // Always check isConditionalPropExist for hide, before to call this function 
  // Because This function assumes that renderData[elmId].metaData.conditionalMeta.hide exists
  const meta = renderData[elmId].metaData;
  const expression = meta.conditionalMeta.hide.trim();

  const isElmHidden = common.parseEval(expression, formData);

  return {
    renderData: {...renderData, [elmId]: {...renderData[elmId], isHidden: isElmHidden}},
    formData: {...formData, [elmId] : isElmHidden ? '' : formData[elmId]}
  };
}

export const updateConditionalFormData = (renderData, formData, fieldId) => {
  const newFormData = {...formData};
  let expression, 
      conditionalMeta = renderData[fieldId].metaData.conditionalMeta;

  if (conditionalMeta && conditionalMeta.default && conditionalMeta.default !== '') {
    expression = conditionalMeta.default;
  }
  if (expression) {
    newFormData[fieldId] = common.parseEval(expression, newFormData);
  }

  return { renderData, formData: {...newFormData}}
}

// Update the hidden fields based on 'display-on-selection' fields
export const updateHiddenFields = (renderData, formData, fieldOrder) => {
  let updatedData = { renderData: { ...renderData }, formData: {...formData} };
  fieldOrder.forEach((fieldId) => {
    const meta = renderData[fieldId].metaData;
    // Set element's renderData isHidden if meta.hide is true
    if (meta.hide) {
      updatedData.renderData[fieldId].isHidden = true;
    }
    // If there is a condition exists to hide field in meta 
    // Set it hidden
    if (isConditionalPropExist(renderData, fieldId, 'hide')) {
      updatedData = performConditionalHideOperation(updatedData.renderData, updatedData.formData, fieldId);
    }
    // Below call is to modify the value of other form fields if they are conditionally determined
    if (isConditionalPropExist(renderData, fieldId, 'default')) {
      updatedData = updateConditionalFormData(updatedData.renderData, updatedData.formData, fieldId);
    }
    // Below call is for display-on-selection functionality 
    // Its a bonus functionality for enum fields when selection of a particular value, displays the certain otherwise kept hidden fields
    updatedData = setHiddenOnSelection(updatedData.renderData, fieldId, updatedData.formData);
  });

  return { ...updatedData };
};

// Set hidden flag based on 'display-on-selection' object
export const setHiddenOnSelection = (renderData, elmId, formData) => {
  const meta = renderData[elmId].metaData;
  const displayConfig = meta['display-on-selection'];
  if (displayConfig && !renderData[elmId].isHidden) {
    const unionFormIds = meta.dependentFields;
    unionFormIds.forEach((fieldId) => {
      if (renderData[fieldId]) {
        renderData[fieldId].isHidden = true;
        // const defaultValue = getDefaultFormData(fieldId);
        // formData[fieldId] = defaultValue || '';
        const dependentFieldMeta = renderData[fieldId].metaData;
        if (dependentFieldMeta['display-on-selection']) {
          const updatedData = setHiddenOnSelection(renderData, fieldId, formData);
          renderData = { ...updatedData.renderData };
          formData = { ...updatedData.formData };
        }
      } else {
        window.logger.error('Meta', `Missing Field ${fieldId} in ${elmId}`, renderData);
      }
    });

    if (formData[elmId] !== undefined && formData[elmId] !== '' && formData[elmId] !== null) {
      if (!displayConfig[formData[elmId]]) { console.debug('displayConfig doesnt exist for', formData[elmId]); }
      console.debug('what is inside isHidden', formData[elmId], displayConfig, displayConfig, displayConfig[formData[elmId].toString()]);
      if (common.isJson(formData[elmId]) && Array.isArray(JSON.parse(formData[elmId]))) {
        const valArr = JSON.parse(formData[elmId]);
        valArr.forEach((value) => {
          if (displayConfig[value]) {
            displayConfig[value].forEach((fieldId) => {
              if (renderData[fieldId]) {
                renderData[fieldId].isHidden = false;
              } else {
                window.logger.error('Meta', `Missing Field ${fieldId} in ${elmId}`, renderData);
              }
            });
          }
        });
      } else if (displayConfig[formData[elmId]]) {
        displayConfig[formData[elmId].toString()].forEach((fieldId) => {
          if (renderData[fieldId]) {
            renderData[fieldId].isHidden = false;
          } else {
            window.logger.error('Meta', `Missing Field ${fieldId} in ${elmId}`, renderData);
          }
        });
      }
    }
  }
  return { renderData: { ...renderData }, formData: { ...formData } };
};


export const getComponentByFormType = (formType) => componentMap.componentMapByFormType[formType];

export const getComponentByScreenId = (screenId) => {
  const id = screenId.split('-')[0];
  return componentMap.singleComponentMapByScreen[id];
};

