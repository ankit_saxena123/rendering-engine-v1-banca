import dottie from 'dottie';
import isUndefined from 'lodash/isUndefined';
import isNull from 'lodash/isNull';

export const _restSuccessCallback = (response, api) => {
  let data = response,
    self = this;
  const map = api && api.mapping;

  function _extractSubProps(propStr) {
    return propStr.trim().split('.');
  }

  const deltaFormObject = {};

  if (map) {
    Object.keys(map).forEach((key) => {
      // let responsePropsArray = _extractSubProps(map[key]), // returns an array of series nested props
      //   value = { ...data };
      //     // loop through the props Array to get the right nested value
      // while (responsePropsArray.length >= 1) {
      //   value = value[responsePropsArray.shift()];
      // }
      value = dottie.get(data, map[key]);
      if (isUndefined(value) || isNull(value)) {
        value = '';
      }
      deltaFormObject[key] = value;
      // window.setTimeout(() => {self.props.updateFormData(key, value)}, 0);
    });
  }
  return deltaFormObject;
}

export const getNewMastersOptions = (response, api) => {
  let optionObjs = response.data;
  let optionMap = api && api['options-mapping'];
  const dependentFields = api.dependentFields ? api.dependentFields : {};

  const options = [];

  Array.isArray(optionObjs) && optionObjs.forEach((val) => {
    const optionObj = {
      id: val[optionMap.id],
      name: val[optionMap['name-index']],
      dependentFields: {}
    }

    Object.keys(dependentFields).forEach((field) => {
      optionObj.dependentFields[field] = val[dependentFields[field]] ? val[dependentFields[field]] : '';
    });

    options.push(optionObj);
  })

  return options;

}

export const getOldMasterOptions = (response, api) => {
  let optionObjs = response.response && response.response.values,
    // This optionMap contains the index of values to be displayed for "name" of the option
    // && the identifier key for "id" of the option
    optionMap = api && api['options-mapping'];
  const options = [];
  const dependentFields = api.dependentFields ? api.dependentFields : {};
  Array.isArray(optionObjs) && optionObjs.forEach((val) => {
    const optionObj = {
      id: typeof(optionMap.id) === 'string' && optionMap.id === 'id' ? val[optionMap.id] : val.values[parseInt(optionMap['id'])],
      name: val.values[parseInt(optionMap['name-index'])],
      dependentFields: {},
    };
    Object.keys(dependentFields).forEach((field) => {
      optionObj.dependentFields[field] = val.values[dependentFields[field]] ? val.values[dependentFields[field]] : '';
    });
    options.push(optionObj);
  });

  return options;
}



export const populateOptionsCallback = (response, api) => {
  let options = [];

  if (api.apiSource === 'newMasters') {
    options = getNewMastersOptions(response, api);
  } else {
    options = getOldMasterOptions(response, api);
  }
  // Setting up the unique options in dropdown
  // Filter it on the unique ids
  const idMap = {};
  options.forEach((option) => {
    idMap[option.id] = option;
  });
  let optionsL = Object.keys(idMap).map((id) => idMap[id]);

  return optionsL;
}


export const buildValueObject = (data, formData, userDetails) => {
  let payloadSample = data,
      payloadData = {};
  let formDataKeys = Object.keys(formData);

  Object.keys(payloadSample || []).forEach((key) => {
    if (typeof payloadSample[key] === 'object') {
      payloadData[key] = buildValueObject(payloadSample[key], formData, userDetails);
    } else {
      if (formDataKeys.indexOf(payloadSample[key]) !== -1) {
        const formDataValue = formData[payloadSample[key]];
        payloadData[key] = formDataValue || '';
      }
      else if (key === 'process_instance_id' || key === 'processInstanceId') {
        payloadData[key] = userDetails && userDetails.processInstanceId;
      } else {
        // In this case, pass the hard-coded value provided in meta.api.data itself
        payloadData[key] = payloadSample[key];
      }
    }
  });

  return payloadData;
}

export const _buildElasticPayloadData = (value, api) => {
  const self = this;
  if (api) {
    let payloadSample = api.data,
      payloadData = {
        index: payloadSample.index,
        query_params: {},
      },
      prefixFields = api['prefix-id'] || [];

    Object.keys(payloadSample).forEach((key) => {
          // insert the values from formData mapped to the keys
      if (key === 'process_instance_id' || key === 'processInstanceId') {
        payloadData.query_params[key] = self.props.userDetails && self.props.userDetails.processInstanceId;
      } else if (payloadSample[key] === self.props.elmId) {
        let formDataValue = '*';
        if (prefixFields) {
          prefixFields.forEach((field) => {
            const fieldValue = (self.props.formData[field] !== 'invalid') ? self.props.formData[field] : '';
            formDataValue += `${fieldValue} `;
          });
        }
        formDataValue += `${value}*`;
        payloadData.query_params[key] = formDataValue || '';
      } else if (key !== 'index') {
        payloadData.query_params[key] = ((self.props.formData[payloadSample[key]] !== 'invalid') && !!self.props.formData[payloadSample[key]]) ? self.props.formData[payloadSample[key]] : '';
      }
    });
    return payloadData;
  }
  return undefined;
}

const getFormattedValueForFilter = (value, comparator) => {
  let formattedValue = value;
  if (comparator && comparator === '=') {
    // In case of '=' comparator the value is supposed to go as it is
    formattedValue = value ? "'" + value + "'" : "'" + "'";
  } else if (comparator && comparator === 'like') {
    // In case of "like" comparator
    // the backend will do a pattern check with the value provided inside '%%' 
    formattedValue = value ? "'%" + value + "%'" : "'%" + "%'";
  }
  return formattedValue;
}

export const buildNewMastersPayload = (data, formData, userDetails, elmId, elmVal) => {
  let payloadSample = data,
      payloadData = Array.isArray(data) ? [] : {};
  let formDataKeys = Object.keys(formData);
  /*
    Expected payload for the masters API (for filtering) is supposed to be in the below format
    {
      "keyList": [
          {
              "isCaseSensitive": false,
              "key": "city",
              "value": "'%ch%'",
              "isLimit": true,
              "range": 5,
              "comparator": "like",
              "operator": ""
          }
      ]
    }
  */

  Object.keys(payloadSample || []).forEach((key) => {
    if (typeof payloadSample[key] === 'object') {
      payloadData[key] = buildNewMastersPayload(payloadSample[key], formData, userDetails, elmId, elmVal);
    } else {
      if (formDataKeys.indexOf(payloadSample[key]) !== -1) {
        let formDataValue;
        if (elmId && (elmId === payloadSample[key]) && elmVal) {
          formDataValue = elmVal !== 'invalid' ? elmVal : '';
        } else {
          formDataValue = formData[payloadSample[key]] !== 'invalid' ? formData[payloadSample[key]] : '';
        }
        if (key === 'value') {
          payloadData[key] = getFormattedValueForFilter(formDataValue, payloadSample['comparator']);
        } else {
          payloadData[key] = formDataValue || '';
        }
      }
      else if (key === 'process_instance_id' || key === 'processInstanceId') {
        payloadData[key] = userDetails && userDetails.processInstanceId;
      } else {
        // In this case, pass the hard-coded value provided in meta.api.data itself
        if (key === 'value') {
          payloadData[key] = getFormattedValueForFilter(payloadSample[key], payloadSample['comparator']);
        } else {
          payloadData[key] = payloadSample[key];
        }
      }
    }
  });

  return payloadData;
}

export const _buildPayloadData = (api, formData, userDetails, elmId, val) => {
  if (api) {
    let payloadSample = api.data,
        payloadData = {};
    let formDataKeys = Object.keys(formData);

    if (api.apiSource === 'newMasters') {
      payloadData = buildNewMastersPayload(payloadSample, formData, userDetails, elmId, val);
    } else {
      payloadData = buildValueObject(payloadSample, formData, userDetails);
    }
    return payloadData;
  }

  return undefined;
}


export const findPayloadFormDataKeys = (data, formData) => {
  let formDataKeys = Object.keys(formData);
  const keys = [];
  Object.keys(data).forEach((key) => {
    if (typeof data[key] === 'object') {
      let childrenFormDataKeys = findPayloadFormDataKeys(data[key], formData);
      Array.prototype.push.apply(keys, childrenFormDataKeys);
    } else {
      // If the payload is dependent on some other form key data
      if (formDataKeys.indexOf(data[key]) !== -1) {
        keys.push(data[key]);
      }
    }
  });

  return [...keys];
}