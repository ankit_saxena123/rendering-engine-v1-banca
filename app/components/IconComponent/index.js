import React from 'react';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import ActionDelete from 'material-ui/svg-icons/action/delete';
import styleVars from 'configs/styleVars';
const fonts = styleVars.fonts,
  colors = styleVars.colors;

export default class IconComponent extends React.PureComponent {

  onChangeHandler = (e) => {
    this.props.onChangeHandler && this.props.onChangeHandler(e.target.value, this.props.elmId);
  }

  render() {
    const { compProps } = { ...this.props };
    const color = !compProps.disabled ? colors.primaryBGColor : colors.primaryDisableColor;
    const iconStyles = { pointerEvents: compProps.disabled ? 'none' : 'auto', cursor: compProps.disabled ? 'not-allowed' : 'default' };
    switch (compProps.type) {
      case 'delete':
        return (
          <ActionDelete
            color={color}
            style={iconStyles}
            onClick={(e) => this.onChangeHandler(e, this.props.elmId)}
          />
        );
      default:
        return null;
    }
  }
}
