/**
*
* RenderAgreement
*
*/

import React from 'react';
import { View, Modal, Image, WebView, TouchableOpacity } from 'react-native';
import RenderError from '../RenderError/';
import Text from '../RenderText';
import Icon from 'react-native-vector-icons/MaterialIcons';
import RenderButton from '../RenderButton/';
import RenderAgreementAbstract from './RenderAgreementAbstract';
import * as styles from './styles';
import arrowIcon from '../../images/rightArrow.png';
import docIcon from '../../images/doc-icon.png';

export default class RenderAgreementComponent extends RenderAgreementAbstract { // eslint-disable-line react/prefer-stateless-function

  componentWillUnmount() {
    this.setState({ showAgreement: false });
  }

  toggleModal = (value) => {
    this.setState({ showAgreement: value });
    this.getDocuSignUrl();
  }

  onLoad = (state) => {
    console.debug('inside onLoad', state, this.props.compProps.redirectUrl);
    if (state.url.includes(this.props.compProps.redirectUrl)) {
      this.handleRedirection(state.url);
      this.toggleModal(false);
    }
  }

  render() {
    const { showAgreement } = this.state;
    const { label, value, error, docusignUrlKey } = this.props.compProps;
    // TODO: Need to change implementation once api to fetch url is done
    const url = this.state.docusignURL;// this.props.formData[docusignUrlKey];
    if (!!url && !value && showAgreement) {
      return (
        <Modal
          visible={showAgreement}
          animationType="slide"
          onRequestClose={() => this.toggleModal(false)}
        >
          <WebView
            startInLoadingState
            scalesPageToFit
            onNavigationStateChange={this.onLoad}
            style={{ flex: 1 }}
            source={{ uri: url }}
          />
        </Modal>
      );
    }
    // Error Component
    const errorMessage = error ? (<RenderError errorMessage={error} />) : null;
    return (
      <View>
        <View>
          <TouchableOpacity
            onPress={() => this.getDocuSignUrl()}
          >
            <View {...styles.wrapper}>
              <View {...styles.imageWrapper}>
                <Image
                  source={docIcon}
                  {...styles.docIcon}
                />
              </View>
              <Text {...styles.labelText}>{label}</Text>
              <Image
                source={arrowIcon}
                {...styles.arrowIcon}
              />
            </View>
          </TouchableOpacity>
        </View>
        {errorMessage}
      </View>
    );
  }
}
