/**
*
* RenderCheckbox
*
*/

import React from 'react';
// import styled from 'styled-components';

export default class RenderCheckboxAbstract extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.state = {
      checked: this.props.compProps.checked,
    };
    this.onChangeHandler = this.onChangeHandler.bind(this);
    this.onBlurHandler = this.onBlurHandler.bind(this);
  }

  componentWillMount() {

  }

  onChangeHandler(e) {
    this.props.onChangeHandler(e.target.checked, this.props.elmId);
  }

  onBlurHandler(e) {
    this.props.onBlurHandler(e.target.checked, this.props.elmId);
  }

  render() {
    return null;
  }
}
