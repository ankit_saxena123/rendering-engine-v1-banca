/**
*
* RenderRestDropdown
*
*/

import React from 'react';

import * as metaApiHelper from '../../utilities/metaApiHelper';
// import styled from 'styled-components';

export default class RenderRestDropdownAbstract extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      showList: false,
      optionsList: [],
      triggerOnFormChange: false,
      relevantFormDataItems: [],
    };
    this.onChangeHandler = this.onChangeHandler.bind(this);
    this.onBlurHandler = this.onBlurHandler.bind(this);
    this.getOptionData = this.getOptionData.bind(this);
    this.populateOptionsCallback = this.populateOptionsCallback.bind(this);

  }

  componentWillMount() {
    // set basic configuration for component
    const meta = this.props.renderData && this.props.renderData.metaData;
    let formDataKeys = Object.keys(this.props.formData);
    if (meta && meta.api) {
      let payloadSample = meta.api.data;
      // const relevantFormDataItems = [];
      // set the flags for whether this rest fields depends on some other form data to make the API call or not

      let relevantFormDataItems = metaApiHelper.findPayloadFormDataKeys(payloadSample, this.props.formData);
      if (relevantFormDataItems.length) {
        this.setState({
          triggerOnFormChange: true,
          relevantFormDataItems: relevantFormDataItems
        })
      }
    }
  }

  componentDidUpdate = (prevProps) => {
    const meta = this.props.renderData && this.props.renderData.metaData;
    const elmId = this.props.elmId;
    if (this.state.triggerOnFormChange) {
      const updatedFormItems = this.state.relevantFormDataItems.filter((formKey) => {
        return (prevProps.formData[formKey] !== this.props.formData[formKey]) && this.props.journeyRenderData[formKey].isValid;
      });
      const isAnyFormItemInvalid = this.state.relevantFormDataItems.some((formKey) => {
        return !this.props.journeyRenderData[formKey].isValid;
      });
      if(updatedFormItems.length && !isAnyFormItemInvalid) {
        this.callRestElem(meta.api);
      }
    }

    // if an API is supposed to be called on the value update of the dropdown
    // TODO: Handle it in a generic fashion with for all the fields
    if (prevProps.formData[elmId] !== this.props.formData[elmId]) {
      if (meta.apiOnUpdate && typeof (meta.apiOnUpdate) === 'object') {
        // call the rest API with onUpdate configuration
        this.callRestElem(meta.apiOnUpdate);
      }
    }
  }

  componentWillUnmount () {
    this.mounted = false;
  }

  componentDidMount () {
    this.mounted = true;
    const meta = this.props.renderData && this.props.renderData.metaData;
    this.callRestElem(meta.api);
  }

  populateOptionsCallback(response, api) {
    let options = metaApiHelper.populateOptionsCallback(response, api);
    this.setState({ optionsList: options });
    const idMap = {};
    options.forEach((option) => {
      idMap[option.id] = option;
    });
    if (this.props.formData[this.props.elmId]) {
      if (!idMap[this.props.formData[this.props.elmId]]) {
        this.props.updateFormData(this.props.elmId, '');
      }
    }
  }

  _restSuccessCallback = (response, api) => {
    const self = this;
    let deltaUpdatedFormObject = metaApiHelper._restSuccessCallback(response, api);
    Object.keys(deltaUpdatedFormObject).forEach((formKey) => {
      window.setTimeout(() => {self.props.updateFormData(formKey, deltaUpdatedFormObject[formKey])}, 0);
    });
  }

  _buildRequestObject(api) {
    const self = this;
    const reqData = {
      url: api && api.url,
      params: metaApiHelper._buildPayloadData(api, this.props.formData, this.props.userDetails),
      userDetails: this.props.userDetails,
      successCb: (response) => {
        if (this.mounted) {
          if (api.mapping && typeof(api.mapping) === 'object') {
            if (Object.keys(api.mapping).length) {
              self._restSuccessCallback(response, api);
            }
          } 
          if (api['options-mapping'] && typeof(api['options-mapping']) === 'object') {
            // default configuration for an API is to populateOptions
            // Setup the render Data in the app
            // Call the render saga service
            if (Object.keys(api['options-mapping']).length) {
              self.populateOptionsCallback(response, api);
            }
          }
        }
      },
      errorCb: () => {
      
      },
    };
    return reqData;
  }

  callRestElem(api) {
    if (typeof api === 'object') {
      const method = api && api.method,
            reqObj = this._buildRequestObject(api);

      if (method === 'POST') {
        this.props.postRequest({ key: null, data: reqObj });
      } else {
        this.props.getRequest({ key: null, data: reqObj });
      }
    }
  }

  getOptionData() {

  }

  onChangeHandler(e, index, value) {
    this.props.onChangeHandler(value, this.props.elmId);
  }

  onBlurHandler(e, index, value) {
    this.props.onBlurHandler(value, this.props.elmId);
  }

  render() {
    return null;
  }
}

