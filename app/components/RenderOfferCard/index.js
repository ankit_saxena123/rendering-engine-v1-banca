/**
*
* RenderOfferCard
*
*/

import React from 'react';
import RenderOfferCardComponent from './RenderOfferCardComponent';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

class RenderOfferCard extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <RenderOfferCardComponent {...this.props} />
    );
  }
}

RenderOfferCard.propTypes = {

};

export default RenderOfferCard;
