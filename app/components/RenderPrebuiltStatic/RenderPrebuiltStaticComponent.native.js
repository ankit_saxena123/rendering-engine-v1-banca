/**
*
* RenderInputText
*
*/

import React from 'react';
import { View, Dimensions } from 'react-native';

import HTML from 'react-native-render-html';

import RenderPrebuiltStaticAbstract from './RenderPrebuiltStaticAbstract';
import { classesStyles, baseFontStyle } from './classStyles';

export default class RenderPrebuiltStaticComponent extends RenderPrebuiltStaticAbstract { // eslint-disable-line react/prefer-stateless-function
  render() {
    // <p class='loan-info'>PD - Residence journey is successfully completed.</p><p class='thank-heading'>Thank You</p>
    return (<View>
      <HTML
        baseFontStyle={baseFontStyle}
        classesStyles={classesStyles}
        html={(this.props.renderData.message && this.props.renderData.message.value) || this.props.renderData.value}
        imagesMaxWidth={Dimensions.get('window').width}
      />
    </View>);
  }
}

/* Received props are
All the default styling
Behavioural attributes

onChange Fn
onBlur Fn
*/
