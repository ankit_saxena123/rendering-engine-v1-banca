/**
*
* RenderRestInput
*
*/

import React from 'react';
// import styled from 'styled-components';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import * as appActions from '../../containers/AppDetails/actions';
import { makeSelectUserDetails } from '../../containers/AppDetails/selectors';
import { makeSelectRenderData } from '../../containers/RenderJourney/selectors';

import RenderRestInputComponent from './RenderRestInputComponent';

class RenderRestInput extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function


  // TODO:
  // This rest component just support the Input text for now
  // Make is supported for all other form input types
  // Use form_type for choosing the right type
  render() {
    return (
      <RenderRestInputComponent {...this.props} />
    );
  }
}

RenderRestInput.propTypes = {

};

const mapStateToProps = createStructuredSelector({
  userDetails: makeSelectUserDetails(),
  journeyRenderData: makeSelectRenderData(),
});

function mapDispatchToProps(dispatch) {
  return {
    getRequest: (reqObject) => dispatch(appActions.getRequest(reqObject)),
    postRequest: (reqObject) => dispatch(appActions.postRequest(reqObject)),
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

// export default RenderRestInput;
export default compose(
  withConnect,
)(RenderRestInput);
