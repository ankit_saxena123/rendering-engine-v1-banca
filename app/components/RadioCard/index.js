/**
*
* RadioField
*
*/

import React from 'react';
// import styled from 'styled-components';
import { Card, CardActions, CardHeader, CardMedia, CardTitle, CardText } from 'material-ui/Card';
import { RadioCardLabel, RadioCardsWrapper, RadioCardWrapper, selectedCardHeaderStyles, selectedTextStyles, imageStyle, cardMediaStyle } from './RadioCardStyles';

import RenderError from '../RenderError/';
import styledComponents from '../../configs/styledComponents';
import Row from '../../components/Row';
import Column from '../../components/Column';
import { layoutConfig } from '../../configs/appConfig';
import RenderHeading from '../../components/RenderHeading';

export default class RadioCardField extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  onChangeHandler = (e, value) => {
    this.props.onChangeHandler && this.props.onChangeHandler(e, value);
  };

  getRadioCard = (elmId, { id, name, desc, imageUrl }, selectedValue) => {
    const selected = id === selectedValue;
    const headerStyles = selected ? selectedCardHeaderStyles : {};
    const titleStyle = selected ? selectedTextStyles : {};
    return (
      <RadioCardWrapper selected={selected} >
        <Card
          id={'radioCard-'+elmId+'-'+id}
          onClick={
            () => {
              this.onChangeHandler(elmId, id);
            }
          }
          key={'radio-'+elmId+'-'+id}
        >
          <CardHeader title={name} style={headerStyles} titleStyle={titleStyle} />
          <CardMedia style={cardMediaStyle}>
            <img style={imageStyle} src={imageUrl} alt={name} />
          </CardMedia>
          <CardText> {desc} </CardText>
        </Card>
      </RadioCardWrapper>
    );
  };

  render() {
    const { errorMessage, componentProps : { radioGroupProp: { valueSelected, cardsPerRow } }, className, options, elemId, label,  } = this.props;
    let error = null,
      radioCards = [],
      radioCardsMap = [];
    const colWidth = layoutConfig.defaultRadioCardsWrapperWidth / cardsPerRow;
    if (errorMessage) {
      error = (<RenderError errorMessage={errorMessage} />);
    }
    if (options && options.length) {
      options.forEach((option, index) => {
        radioCards.push(<Column colWidth={colWidth}>
          {this.getRadioCard(elemId, option, valueSelected)}
        </Column >
        );
        if (index + 1 !== 1 && (index + 1) % cardsPerRow === 0) {
          radioCardsMap.push(<Row key={'row-'+index}> {[...radioCards]} </Row>);
          radioCards.length = 0;
        }
      });
      if (radioCards.length) radioCardsMap.push(<Row key={'row-'+(options.length+1)}> {[...radioCards]} </Row>);
    }
    const hintText = styledComponents.getStyledHintText(this.props.hintText);

    return (
      <div className={className}>
        <RenderHeading
          compProps={{
            label,
            type: 'h4',
          }}
        />
        {hintText}
        <RadioCardsWrapper>
            { radioCardsMap }
        </RadioCardsWrapper>
        {error}
      </div>
    );
  }
}

RadioCardField.propTypes = {

};

RadioCardField.defaultProps = {
};

