/*
 * Loader Messages
 *
 * This contains all the text for the Loader component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.Loader.header',
    defaultMessage: 'This is the Loader component !',
  },
});
