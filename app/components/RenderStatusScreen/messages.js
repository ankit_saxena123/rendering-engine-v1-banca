/*
 * RenderStatusScreen Messages
 *
 * This contains all the text for the RenderStatusScreen component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.RenderStatusScreen.header',
    defaultMessage: 'This is the RenderStatusScreen component !',
  },
});
