/**
*
* RenderMyinfo
*
*/

import React from 'react';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

import RenderMyinfoComponent from './RenderMyinfoComponent';

class RenderMyinfo extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <RenderMyinfoComponent {...this.props} />
    );
  }
}

RenderMyinfo.propTypes = {

};

export default RenderMyinfo;
