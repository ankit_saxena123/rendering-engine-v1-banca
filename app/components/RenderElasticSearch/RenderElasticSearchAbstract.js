/**
*
* RenderElasticSearch
*
*/

import React from 'react';
// import styled from 'styled-components';
import * as common from '../../utilities/commonUtils';
import * as autoCompleteHelper from '../../utilities/autoCompleteHelper';
import * as metaApiHelper from '../../utilities/metaApiHelper';

export default class RenderElasticSearchAbstract extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      inputVal: props.compProps && props.compProps.value,
      optionsList: [],
      showList: false,
      loaderState: {},
    };
    this.onChangeHandler = this.onChangeHandler.bind(this);
    this.onBlurHandler = this.onBlurHandler.bind(this);
    this.populateOptionsCallback = this.populateOptionsCallback.bind(this);
    // this._buildPayloadData = this._buildPayloadData.bind(this);
    this._buildRequestObject = this._buildRequestObject.bind(this);
    // this.callRestElem = common.debounce(this.callRestElem, 500);
    this.setListFlag = this.setListFlag.bind(this);
  }

  componentWillReceiveProps(newProps) {
    if (newProps && newProps !== this.props && newProps.compProps.value !== 'invalid') {
      this.setState({ inputVal: newProps.compProps.value });
    }
  }

  componentWillMount() {
    // set basic configuration for component
    const meta = this.props.renderData && this.props.renderData.metaData;
    let formDataKeys = Object.keys(this.props.formData);
    if (meta && meta.api) {
      let payloadSample = meta.api.data;
      // const relevantFormDataItems = [];
      // set the flags for whether this rest fields depends on some other form data to make the API call or not

      let relevantFormDataItems = metaApiHelper.findPayloadFormDataKeys(payloadSample, this.props.formData);
      if (relevantFormDataItems.length) {
        this.setState({
          triggerOnFormChange: true,
          relevantFormDataItems: relevantFormDataItems
        })
      }
    }
  }

  componentDidUpdate = (prevProps) => {
    const meta = this.props.renderData && this.props.renderData.metaData;
    const elmId = this.props.elmId;
    if (this.state.triggerOnFormChange) {
      const updatedFormItems = this.state.relevantFormDataItems.filter((formKey) => {
        return (prevProps.formData[formKey] !== this.props.formData[formKey]) && this.props.journeyRenderData[formKey].isValid;
      });
      const isAnyFormItemInvalid = this.state.relevantFormDataItems.some((formKey) => {
        return !this.props.journeyRenderData[formKey].isValid;
      });
      if(updatedFormItems.length && !isAnyFormItemInvalid) {
        this.callRestElem(meta.api);
      }
    }

    // if an API is supposed to be called on the value update of the dropdown
    // TODO: Handle it in a generic fashion with for all the fields
    if (prevProps.formData[elmId] !== this.props.formData[elmId]) {
      if (meta.apiOnUpdate && typeof (meta.apiOnUpdate) === 'object') {
        // call the rest API with onUpdate configuration
        this.callRestElem(meta.apiOnUpdate);
      }
    }
  }

  onChangeHandler() {
    this.debounceFunc();
  }
  
  debounceFunc = common.debounce(() => {
    const { inputVal: value } = this.state;
    if (!!value) {
      const meta = this.props.renderData && this.props.renderData.metaData;
      const formData = this.props.formData;
      const dependentFields = (meta.api && meta.api.dependentFields) ? meta.api.dependentFields : {};
      autoCompleteHelper.onChangeHandler(this, value, dependentFields, formData);
      this.callRestElem(meta.api);
    }
  }, 500);


  setListFlag(flag) {
    this.setState({ showList: flag });
  }

  _buildRequestObject(api, value) {
    const self = this;
    const elmId = this.props.elmId;
    const reqData = {
      url: api && api.url,
      params: metaApiHelper._buildPayloadData(api, this.props.formData, this.props.userDetails, elmId, value),
      loader: true,
      userDetails: this.props.userDetails,
      successCb: (response) => {
        self.setState({ loaderState: self.buildLoaderObj(false) });
        if (api.mapping && typeof(api.mapping) === 'object') {
          if (Object.keys(api.mapping).length) {
            self._restSuccessCallback(response, api);
          }
        } 
        if (api['options-mapping'] && typeof(api['options-mapping']) === 'object') {
          // default configuration for an API is to populateOptions
          // Setup the render Data in the app
          // Call the render saga service
          if (Object.keys(api['options-mapping']).length) {
            self.populateOptionsCallback(response, api);
          }
        }
      },
      errorCb: () => {
        self.setState({ loaderState: self.buildLoaderObj(false) });
      },
    };
    return reqData;
  }

  // _buildRequestObject(value, api) {
  //   const self = this;
  //   const reqData = {
  //     url: api && api.url,
  //     params: this._buildPayloadData(value, api),
  //     loader: true,
  //     userDetails: this.props.userDetails,
  //     successCb: (response) => {
  //       self.setState({ loaderState: self.buildLoaderObj(false) });
  //       // Setup the render Data in the app
  //       // Call the render saga service

  //       if (api['options-mapping'] && typeof(api['options-mapping']) === 'object') {
  //         // default configuration for an API is to populateOptions
  //         // Setup the render Data in the app
  //         // Call the render saga service
  //         if (Object.keys(api['options-mapping']).length) {
  //           self.populateOptionsCallback(response, api);
  //         }
  //       }
  //     },
  //     errorCb: () => {
  //       self.setState({ loaderState: self.buildLoaderObj(false) });
  //     },
  //   };
  //   return reqData;
  // }

  _restSuccessCallback = (response, api) => {
    const self = this;
    let deltaUpdatedFormObject = metaApiHelper._restSuccessCallback(response, api);
    Object.keys(deltaUpdatedFormObject).forEach((formKey) => {
      window.setTimeout(() => {self.props.updateFormData(formKey, deltaUpdatedFormObject[formKey])}, 0);
    });
  }

  buildLoaderObj = (flag) => ({
    isVisible: flag,
    loader: true,
  })

  // callRestElem = (api) => {
  //   const { inputVal: value } = this.state;
  //   if (value && typeof api === 'object') {
  //     const meta = this.props.renderData && this.props.renderData.metaData;
  //     const method = meta.api && meta.api.method;
  //     const reqObj = this._buildRequestObject(value, meta.api);
  //     if (method === 'POST') {
  //       this.setState({ loaderState: this.buildLoaderObj(true) });
  //       this.props.postRequest({ key: null, data: reqObj });
  //     } else {
  //       this.setState({ loaderState: this.buildLoaderObj(true) });
  //       this.props.getRequest({ key: null, data: reqObj });
  //     }
  //   }
  // }

  callRestElem = (api) => {
    if (typeof api === 'object') {
      const { inputVal: value } = this.state;
      const meta = this.props.renderData && this.props.renderData.metaData,
        method = api && api.method;
      const reqObj = this._buildRequestObject(api, value);
      if (method === 'POST') {
        this.props.postRequest({ key: null, data: reqObj });
      } else if (method === 'GET') {
        this.props.getRequest({ key: null, data: reqObj });
      }
    }
  }

  populateDependantFields(option) {
    autoCompleteHelper.populateDependantFields(this, option);
  }

  populateOptionsCallback(response, api) {
    let options = metaApiHelper.populateOptionsCallback(response, api);
    this.setState({ optionsList: options, showList: true });
  }

  updateOption = (option) => {
    autoCompleteHelper.updateOption(this, option);
  }

  onBlurHandler(e, index, value) {
    this.props.onBlurHandler(value, this.props.elmId);
  }

  render() {
    return null;
  }
}

