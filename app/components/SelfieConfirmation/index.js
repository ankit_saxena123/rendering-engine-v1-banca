/**
*
* SelfieConfirmation
*
*/

import React from 'react';
import SelfieConfirmationComponent from './SelfieConfirmationComponent';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';

class SelfieConfirmation extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <SelfieConfirmationComponent {...this.props} />
    );
  }
}

SelfieConfirmation.propTypes = {

};

export default SelfieConfirmation;
