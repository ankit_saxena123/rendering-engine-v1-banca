/**
 *
 * Asynchronously loads the component for SelfieConfirmation
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
