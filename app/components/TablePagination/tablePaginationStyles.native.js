import { colors, fonts, sizes, lineHeight } from '../../configs/styleVars';
export const paginationButtonsWrapper = {
  flex: 1,
  flexDirection: 'row',
  justifyContent: 'space-around',
  paddingEnd: 30,
};

export const paginationIconWrapper = {
  width: 40,
  height: 40,
  // borderRadius: 2,
  // borderColor: colors.tableRowBG,
  // borderBottomWidth: 0,
  // shadowColor: colors.tableRowBG,
  // shadowOffset: { width: 0, height: 2 },
  // shadowOpacity: 0.8,
  // shadowRadius: 2,
  elevation: 1,
  alignItems: 'center',
  padding: 5,
  backgroundColor: 'transparent',
};

export const paginationIcon = {
  height: '100%',
};
