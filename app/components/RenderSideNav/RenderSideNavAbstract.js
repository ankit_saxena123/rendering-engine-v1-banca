/**
*
* RenderSideNavAbstract
*
*/

import React from 'react';
// import styled from 'styled-components';

export default class RenderSideNavAbstract extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  componentWillMount = () => {
    if (this.props.labels) {
      this.setState({
        currentActiveIndex: this.props.labels.indexOf(this.props.currentActiveStep),
        stepCount: this.props.labels.length,
      });
    }
  }

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.labels) {
      this.setState({
        currentActiveIndex: nextProps.labels.indexOf(nextProps.currentActiveStep),
        stepCount: nextProps.labels && nextProps.labels.length,
      });
    }
  }

  onPress = (index) => {
    if (index < this.state.currentActiveIndex) {
      const { defaultScreenList, backTrigger } = this.props;
      const backScreenKey = Array.isArray(defaultScreenList) && defaultScreenList[index];
      if (backScreenKey) {
        backTrigger(backScreenKey);
      }
    }
  }

  onChangeHandler = (e, value) => {
    this.props.onChangeHandler(value, this.props.elmId);
  }

  render() {
    return null;
  }
}

RenderSideNavAbstract.propTypes = {

};
