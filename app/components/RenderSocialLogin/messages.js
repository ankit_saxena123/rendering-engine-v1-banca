/*
 * RenderSocialLogin Messages
 *
 * This contains all the text for the RenderSocialLogin component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.RenderSocialLogin.header',
    defaultMessage: 'This is the RenderSocialLogin component !',
  },
});
