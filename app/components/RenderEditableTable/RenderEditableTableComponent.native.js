/**
 *
 * RenderEditableTableComponent
 *
 */

import React from 'react';
import { View, ScrollView, TouchableOpacity } from 'react-native';
import { Table, TableWrapper, Cell } from 'react-native-table-component';
import Text from '../RenderText';
import componentProps from '../../containers/RenderStep/componentProps';
import * as styles from './tableStyles';
import RenderButton from '../RenderButton';
import RenderError from '../RenderError';
import RenderHeading from '../RenderHeading';
import MultiInstance from '../../components/MultiInstance'

import styledComponents from "../../configs/styledComponents";
import * as componentMap from "../../utilities/componentMap";
import dottie from "dottie";
import * as common from '../../utilities/commonUtils';

class RenderEditableTableComponent extends MultiInstance { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
  }

  renderTableHeader = () => {
    const { journeyRenderData } = this.props;
    const { fieldMap, fieldOrder } = this.state;
    let fieldHeaders = [];
    if(fieldOrder.length) {
      fieldHeaders = fieldOrder.map((instanceKey, index) => {
        let headerColumnWrapperStyle = this.getHeaderCellStyle(journeyRenderData[instanceKey]);
        const cellData = (<Text>{fieldMap[instanceKey]}</Text>);
        return (<Cell style={headerColumnWrapperStyle} key={`cell-header${index}`} data={cellData} />); // `
      });
    }
    return [...fieldHeaders,...(this.getActionHeaders())];
  };

  getActionHeaders = () => {
    const { actions } = this.state;
    const actionHeaders = [];
    actions.forEach((action,index) => {
      const cellStyle =  this.getHeaderCellStyle({metaData: {}});
      const cellData = (<Text style={styles.headerColumnWrapperStyle}>{action.headerLabel}</Text>);
      action.shouldRender() && actionHeaders.push(<Cell key={`cell-header-action-${index}`} data={cellData} style={[styles.text, cellStyle]} />); // `
    });
    return actionHeaders;
  };

  getActionCells = (key) => {
    const { actions } = this.state;
    let actionCells = [];
    actions.forEach((action,index) => {
      const cellStyle =  this.getHeaderCellStyle({metaData: {}});
      action.shouldRender() &&
      actionCells.push(
          <Cell
              key={`cell-header-action-${index}`}
              data={<View>
                <Text onPress={
                  () => { action.onTrigger(key) }}>{styledComponents.getLinkStyledText(action.name)}</Text></View
              >}
              style={[styles.text, cellStyle]}
          /> // `
      );
    });
    return actionCells;
  };

  getFieldCellStyle = (renderData, isErrored) => {
    const { metaData: {fixedLayout, colWidth} } = renderData;
    let widthObject = !fixedLayout ?  { width: styles.colWidth * colWidth || styles.colWidth} : {};
    return {
      ...styles.headerColumnWrapperStyle,
      ...widthObject,
      ...(isErrored ? styles.cellColumnErrorStyle: {})
    };
  };

  getHeaderCellStyle = (renderData) => {
    const { metaData: {fixedLayout, colWidth} } = renderData;
    let widthObject = !fixedLayout ?  { width: styles.colWidth * colWidth || styles.colWidth} : {};
    return {
      ...styles.headerColumnWrapperStyle,
      ...widthObject,
    };
  };

  renderTableRows = () => {
    const { journeyRenderData } = { ...this.props };
    const { instancesData, isCalculationTable } = { ...this.state };
  

    const tableDataRows = instancesData.map(
      (instanceRowData, index) => (
        <TableWrapper key={`tableRow-${index}`} style={styles.row}>
            {this.renderUnitInstance(instanceRowData, journeyRenderData, index)}
        </TableWrapper>) //`
    );
    if (isCalculationTable) {
        const resultTableRow = (
            <TableWrapper key={`tableRow-resultRow`} style={styles.row}>
                { this.getResultTableRow() }
            </TableWrapper> //`
        );
        tableDataRows.push(resultTableRow);
    }

    return tableDataRows;
  }

  getResultTableRow = () => {
        const { journeyRenderData } = { ...this.props };
        const { isCalculationTable, resultRow, instancesData } = this.state;
        let renderCells = [];
        let { metaData: { fields }} = this.props.renderData;
        fields.forEach((field) => {
            const { operationLabel, fieldId} = field;
            const value = journeyRenderData[fieldId].metaData && journeyRenderData[fieldId].metaData.isCurrency ? common.convertCurrencyFormat(resultRow[fieldId]) : resultRow[fieldId];
            const cellColumnWrapperStyle = this.getFieldCellStyle(journeyRenderData[fieldId]);
            renderCells.push(
              <Cell key={`cell${fieldId}-resultRow`} data={operationLabel ? (operationLabel + ' - ' + value) : ('' + value)} style={[styles.text, cellColumnWrapperStyle]} /> 
            );
        });
        return [...renderCells, ...this.getActionResultColumn()];
    }

  getActionResultColumn = () => {
    const { actions } = this.state;
    let actionCells = [];
    actions.forEach((action,index) => {
      const cellStyle =  this.getHeaderCellStyle({metaData: {}});
      action.shouldRender() &&
      actionCells.push(
          <Cell
              key={`cell-header-action-result-${index}`}
              data={<View><Text></Text></View
              >}
              style={[styles.text, cellStyle]}
          /> // `
      );
    });
    return actionCells;
  }


  renderUnitInstance = (instance, data, instanceIndex) => {
    const instanceKeys = this.state.fieldOrder;
    const componentRenderData = [...this.state.instancesRenderData];
    let renderCells = [];
    instanceKeys.forEach((elm, index) => {
      const keyProps = componentRenderData[instanceIndex] && componentRenderData[instanceIndex][elm];
      if (keyProps.isHidden) { return; }
      const meta = typeof keyProps.metaData === 'string' ? JSON.parse(keyProps.metaData) : keyProps.metaData;
      const instanceValue = (instance[elm] || instance[elm] === '') ? instance[elm] : meta.default;
      const formType = meta.form_type;
      const isUploadValue = (formType === 'upload' && !!instanceValue && !!instanceValue.length);
      keyProps.value = isUploadValue && instanceValue? JSON.parse(instanceValue) : instanceValue;
      const elmId = keyProps.id;
      const UnitComponent = componentMap.componentMapByFormType[formType];
      if (UnitComponent) {
        let compProps = componentProps[formType] &&
            componentProps[formType](
                keyProps,
                keyProps.id,
                keyProps.value,
                {...this.props.formData, ...instance}
            );
        if (formType === 'dropdown' || formType === 'text' ||
            formType === 'datepicker' || formType === 'number') {
          compProps.underlineStyle = styles.underlineStyle;
          compProps.label = '';
          compProps.hintText = '';
          compProps.inline = true;
        }
        const { instancesErrorData, instancesData } = this.state;
        const unitErrorObject = dottie.get(instancesErrorData[instanceIndex],`${elmId}`);
        const isErrored = !dottie.get(unitErrorObject,'isValid',true);

        const renderDataProp = { ...keyProps, ...unitErrorObject };
        const componentSection = (
            <View style={styles.cellWrapper}>
              <UnitComponent
                  compProps={{ ...compProps}}
                  key={elmId}
                  elmId={elmId}
                  renderData={keyProps}
                  formData={{ ...instancesData[instanceIndex] }}
                  onChangeHandler={(value, unitKey = elmId) => {
                    this.onChangeHandler(value, unitKey, instanceIndex);
                    // uncomment below call to validate on field data change ,also validate whole idntance and update error state on deletion of a row
                    // this.updateUnitInstanceErrorState(value, unitKey, instanceIndex);
                  }}
                  // onBlurHandler={value => this.onBlurHandler(value, elmId)}
              />
            </View>
        );
        const cellColumnWrapperStyle = this.getFieldCellStyle(renderDataProp, isErrored);
        renderCells.push(
            <Cell key={`cell${instanceIndex}-${index}`} data={componentSection} style={[styles.text, cellColumnWrapperStyle]} /> // `
        );
      }
    });
    return [...renderCells,...(this.getActionCells(instanceIndex))];
  };

  setLastTouchedFieldData = (event,lastTouchedInstanceIndex, lastTouchedFieldId) => {
    if (lastTouchedFieldId !== this.state.lastTouchedFieldId || lastTouchedInstanceIndex !== this.state.lastTouchedInstanceIndex) {
      const currentTarget = event.currentTarget;
      window.setTimeout(() => {
          this.setState({
              lastTouchedInstanceIndex,
              lastTouchedFieldId,
              openPopover: true,
              anchorEl: currentTarget,
          });
      });
    }
  };

  closePopover = () => {
    this.setState({
      openPopover: false,
    });
  };

  render() {
    const tableHeader = this.renderTableHeader();
    const tableRows = this.renderTableRows();
    const { renderData } = this.props;
    const Wrapper = (renderData.metaData.fixedLayout) ? View : ScrollView;
    let error = null;
    if (this.state.allErrorMessage && this.state.allErrorMessage.length) {
      error = (<RenderError errorMessage={this.state.allErrorMessage} />);
    }
    return (
        <View style={{ marginBottom: 15 }}>
          <RenderHeading
              compProps={{
                label: renderData.name,
                type: 'h3',
              }}
          />
          <Wrapper horizontal style={styles.container}>
            <Table borderStyle={styles.tableBorderStyle}>
              <TableWrapper key={'tableHeader'} style={styles.headerRow}>
                {tableHeader}
              </TableWrapper>
              {tableRows}
            </Table>
          </Wrapper>
          {error}
          {this.isAddAllowed()
              ? <RenderButton
                  type={'primary'}
                  text="+ Add Row"
                  onPress={this.addRow}
              />
              : null
          }
        </View>
    );
  }
}

RenderEditableTableComponent.propTypes = {

};

export default RenderEditableTableComponent;
