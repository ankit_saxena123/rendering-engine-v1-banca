/**
 *
 * RenderEditableTableComponent
 *
 */

import React from 'react';
import { Table, TableBody, TableFooter, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table';
import componentProps from '../../containers/RenderStep/componentProps';
import Popover from 'material-ui/Popover';

import * as styles from './tableStyles';
import RenderButton from '../../components/RenderButton';
import { EditableTableOuterContainer } from './StyleEditableTable';
import RenderHeading from '../RenderHeading';
import MultiInstance from '../../components/MultiInstance'
import * as componentMap from "../../utilities/componentMap";
import dottie from "dottie";
import RenderError from "../RenderError";
import styledComponents from "../../configs/styledComponents";
import * as common from '../../utilities/commonUtils';


class RenderEditableTableComponent extends MultiInstance { // eslint-disable-line react/prefer-stateless-function
    constructor(props) {
        super(props);
    }

    renderTableHeader = () => {
        const { journeyRenderData } = { ...this.props};
        const { fieldMap, fieldOrder } = this.state;
        let fieldHeaders = [];
        if(fieldOrder.length) {
            fieldHeaders = fieldOrder.map((instanceKey, index) => {
                let headerColumnWrapperStyle = this.getHeaderCellStyle(journeyRenderData[instanceKey]);
                return (<TableHeaderColumn
                    style={headerColumnWrapperStyle}
                    key={`cell-header${index}`}>
                    {fieldMap[instanceKey]}
                </TableHeaderColumn>);  //`
            });
        }

        return [...fieldHeaders, ...this.getActionHeaders()];
    };

    getActionHeaders = () => {
        const { actions } = this.state;
        return actions.map((action,index) => (
            action.shouldRender() &&
            (<TableHeaderColumn
                style={styles.headerColumnWrapperStyle}
                key={`cell-header-action-${index}`}>
                {action.headerLabel}
            </TableHeaderColumn>)
        ));
    };

    getActionCells = (key) => {
        const { actions } = this.state;
        return actions.map((action, index) => (
            action.shouldRender() &&
            (<TableHeaderColumn
                style={styles.cellColumnWrapperStyle}
                key={`cell-header-action-${index}`}>
                <span onClick={() => action.onTrigger(key)}>{styledComponents.getLinkStyledText(action.name)}</span>
            </TableHeaderColumn>)
        ));
    };

    getFieldCellStyle = (renderData, isErrored) => {
        const { metaData: {fixedLayout, colWidth} } = renderData;
        let widthObject = !fixedLayout ?  { width: styles.colWidth * colWidth || styles.colWidth} : {};
        return {
            ...styles.cellColumnWrapperStyle,
            ...widthObject,
            ...(isErrored ? styles.cellColumnErrorStyle: {})
        };
    };

    getHeaderCellStyle = (renderData) => {
        const { metaData: {fixedLayout, colWidth} } = renderData;
        let widthObject = !fixedLayout ?  { width: styles.colWidth * colWidth || styles.colWidth} : {};
        return {
            ...styles.headerColumnWrapperStyle,
            ...widthObject,
        };
    };

    getResultTableRow = () => {
        const { journeyRenderData } = { ...this.props };
        const { isCalculationTable, resultRow, instancesData } = this.state;
        let renderCells = [];
        let { metaData: { fields }} = this.props.renderData;
        fields.forEach((field) => {
            const { operationLabel, fieldId} = field;
            const value = journeyRenderData[fieldId].metaData && journeyRenderData[fieldId].metaData.isCurrency ? common.convertCurrencyFormat(resultRow[fieldId]) : resultRow[fieldId];
            const cellColumnWrapperStyle = this.getFieldCellStyle(journeyRenderData[fieldId]);
            renderCells.push(
                <TableRowColumn style={cellColumnWrapperStyle} key={`cell${fieldId}-resultRow`}>
                    <div><span style={{fontWeight: 'bold'}}>{operationLabel ? operationLabel + ' - ' : '' }</span> {value}</div>
                </TableRowColumn>
            );
        });
        return [...renderCells, ...this.getActionResultColumn()];
    }

    getActionResultColumn = () => {
        const { actions } = this.state;
        return actions.map((action, index) => (
            action.shouldRender() &&
            (<TableHeaderColumn
                style={styles.cellColumnWrapperStyle}
                key={`cell-header-action-result-${index}`}>
                <span ></span>
            </TableHeaderColumn>)
        ));
    }

    renderTableRows = () => {
        const { journeyRenderData } = { ...this.props };
        const { instancesData, isCalculationTable } = { ...this.state };
        const tableDataRows = instancesData.map(
            (instanceRowData, index) => (
                <TableRow key={`tableRow-${index}`}>
                    { this.renderUnitInstance(instanceRowData, journeyRenderData, index) }
                </TableRow> //`
            )
        );
        if (isCalculationTable) {
            const resultTableRow = (
                <TableRow key={`tableRow-resultRow`}>
                    { this.getResultTableRow() }
                </TableRow> //`
            );
            tableDataRows.push(resultTableRow);
        }

        return tableDataRows;
    }

    renderUnitInstance = (instance, data, instanceIndex) => {
        const instanceKeys = this.state.fieldOrder;
        const componentRenderData = [...this.state.instancesRenderData];
        let renderCells = [];
        instanceKeys.forEach((elm, index) => {
            const keyProps = componentRenderData[instanceIndex] && componentRenderData[instanceIndex][elm];
            if (keyProps.isHidden) { return; }
            const meta = typeof keyProps.metaData === 'string' ? JSON.parse(keyProps.metaData) : keyProps.metaData;
            const instanceValue = (instance[elm] || instance[elm] === '') ? instance[elm] : meta.default;
            const formType = meta.form_type;
            const isUploadValue = (formType === 'upload' && !!instanceValue && !!instanceValue.length);
            keyProps.value = isUploadValue && instanceValue? JSON.parse(instanceValue) : instanceValue;
            const elmId = keyProps.id;
            const UnitComponent = componentMap.componentMapByFormType[formType];
            if (UnitComponent) {
                let compProps = componentProps[formType] &&
                    componentProps[formType](
                        keyProps,
                        keyProps.id,
                        keyProps.value,
                        {...this.props.formData, ...instance}
                    );
                if (formType === 'dropdown' || formType === 'text' ||
                    formType === 'datepicker' || formType === 'number') {
                    compProps.underlineStyle = styles.underlineStyle;
                    compProps.floatingLabelText = '';
                    compProps.hintText = '';
                }
                const { instancesErrorData, instancesData } = this.state;
                const unitErrorObject = dottie.get(instancesErrorData[instanceIndex],`${elmId}`);
                const unitErrorMessage = dottie.get(unitErrorObject, 'errorMessage');
                const isErrored = !dottie.get(unitErrorObject,'isValid',true);

                const renderDataProp = { ...keyProps, ...unitErrorObject };
                const componentSection = (
                    <div
                        onClick={(event) => {
                            this.setLastTouchedFieldData(event,instanceIndex,elmId);
                        }}>
                        <UnitComponent
                            compProps={{ ...compProps, error: unitErrorMessage }}
                            key={elmId}
                            elmId={elmId}
                            renderData={keyProps}
                            formData={{ ...instancesData[instanceIndex] }}
                            onChangeHandler={(value, unitKey = elmId) => {
                                this.onChangeHandler(value, unitKey, instanceIndex);
                            }}
                            onBlurHandler={value => this.onBlurHandler(value, elmId)}
                        />
                    </div>
                );
                const cellColumnWrapperStyle = this.getFieldCellStyle(renderDataProp,isErrored);
                renderCells.push(
                    <TableRowColumn style={cellColumnWrapperStyle} key={`cell${instanceIndex}-${index}`}>{componentSection}</TableRowColumn>
                );

            }
        });
        return [...renderCells,...this.getActionCells(instanceIndex)];
    };

    setLastTouchedFieldData = (event,lastTouchedInstanceIndex, lastTouchedFieldId) => {
        if (lastTouchedFieldId !== this.state.lastTouchedFieldId || lastTouchedInstanceIndex !== this.state.lastTouchedInstanceIndex) {
            const currentTarget = event.currentTarget;
            window.setTimeout(() => {
                this.setState({
                    lastTouchedInstanceIndex,
                    lastTouchedFieldId,
                    openPopover: true,
                    anchorEl: currentTarget,
                });
            });
        }
    };

    closePopover = () => {
        this.setState({
            openPopover: false,
        });
    };

    render() {
        const { renderData, className } = this.props;
        const tableHeader = this.renderTableHeader();
        const tableRows = this.renderTableRows();
        console.log('fixedLayout', renderData.metaData.fixedLayout);
        const { instancesErrorData } = this.state;
        const errorMessage = dottie.get(instancesErrorData,`${this.state.lastTouchedInstanceIndex}.${this.state.lastTouchedFieldId}.errorMessage`);
        const error = errorMessage && (<RenderError errorMessage= {errorMessage} />);
        return (
            <EditableTableOuterContainer fixedLayout={renderData.metaData.fixedLayout} className={className}>
                <RenderHeading
                    compProps={{
                        label: renderData.name,
                        type: 'h3',
                    }}
                />
                <Table
                    fixedHeader
                    fixedFooter={false}>
                    <TableHeader
                        displaySelectAll={false}
                        adjustForCheckbox={false}
                        enableSelectAll={false}>
                        <TableRow>
                            { tableHeader }
                        </TableRow>
                    </TableHeader>
                    <TableBody
                        displayRowCheckbox={false}
                        showRowHover={false}
                        stripedRows={false}>
                        {tableRows}
                    </TableBody>
                </Table>
                {this.isAddAllowed() ? (
                    <div style={styles.buttonWrapperStyle}>
                        <RenderButton
                            label="+ Add Row"
                            type="primary"
                            onClick={this.addRow}/>
                    </div>) : null}
                <Popover
                    open={this.state.openPopover && !!errorMessage}
                    anchorEl={this.state.anchorEl}
                    canAutoPosition={true}
                    useLayerForClickAway={false}
                    onRequestClose={this.closePopover}
                    style={{paddingTop: 5, paddingLeft: 10,paddingBottom: 5, paddingRight: 10}}
                >
                    { error }
                </Popover>
            </EditableTableOuterContainer>
        );
    }
}

RenderEditableTableComponent.propTypes = {

};

export default RenderEditableTableComponent;
